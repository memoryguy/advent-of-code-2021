package online.wakkawakka.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import online.wakkawakka.model.ExpectedResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {
    private static final Logger logger = LoggerFactory.getLogger(Utility.class);
    private static final Pattern variantPattern = Pattern.compile("^day\\d+_(?<variant>.+)\\.txt$");

    private Utility() {}

    public static BufferedReader getInputFileBuffer(int year, int day, String part, boolean sampleInput, String variant) {
        String filename = String.format("%d/input/%s/day%02d%s%s.txt", year, sampleInput ? "sample" : "test", day, part, variant.isBlank() ? "" : "_" + variant);

        logger.debug("Loading challenge data from {}", filename);

        return getResourceFile(filename);
    }

    public static List<String> getProblemDescription(int year, int day, String part) {
        String filename = String.format("%d/description/day%02d%s.txt", year, day, part);
        List<String> descriptionLines = new ArrayList<>();

        try (BufferedReader description = getResourceFile(filename)) {
            String line;

            if (description != null) {
                while ((line = description.readLine()) != null) {
                    descriptionLines.addAll(formatLine(line));
                }
            }
        } catch (IOException e) {
            logger.warn("Unable to load problem description", e);
        }

        return descriptionLines;
    }

    public static ExpectedResults getExpectedResults(int year) {
        ExpectedResults results = new ExpectedResults();

        try {
            ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
            InputStream inputStream = Utility.class.getClassLoader().getResourceAsStream(String.format("%d/input/expected_results.yml", year));
            results = objectMapper.readerFor(ExpectedResults.class).readValue(inputStream);
        } catch (IOException e) {
            logger.error("Unable to load expected results", e);
        }

        return results;
    }

    public static List<List<String>> getDataVariants(int year, int day) {
        List<String> sampleVariants = new ArrayList<>();
        List<String> testVariants = new ArrayList<>();

        try {
            sampleVariants = getVariants(String.format("%d/input/sample/day%02d*.txt", year, day));
            testVariants = getVariants(String.format("%d/input/test/day%02d*.txt", year, day));
        } catch (IOException e) {
            logger.error("Unable to retrieve list of data variants", e);
        }

        return new ArrayList<>(Arrays.asList(sampleVariants, testVariants));
    }

    private static List<String> getVariants(String pattern) throws IOException {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        List<String> results = new ArrayList<>();

        for (Resource resource : resolver.getResources(pattern)) {
            Matcher matcher = variantPattern.matcher(Objects.requireNonNull(resource.getFilename()));

            if (matcher.find()) {
                String variant = matcher.group("variant");

                logger.debug("Found resource: {}; variant: {}", resource.getFilename(), variant);
                results.add(variant);
            }
        }

        return results;
    }

    private static List<String> formatLine(String line) {
        List<String> result = new ArrayList<>();
        StringBuilder partialLine = new StringBuilder();

        if (line.isEmpty() || line.startsWith("  ")) {
            result.add(line);
        } else {
            Arrays.stream(line.split(" ")).forEach(w -> {
                if ((partialLine.toString() + " " + w).length() > 80) {
                    result.add(partialLine.toString());

                    partialLine.setLength(0);
                    partialLine.append(w);
                } else {
                    partialLine.append(partialLine.length() > 0 ? " " : "").append(w);
                }
            });
        }

        if (partialLine.length() > 0) {
            result.add(partialLine.toString());
        }

        return result;
    }

    private static BufferedReader getResourceFile(String filename) {
        InputStream stream = Utility.class.getClassLoader().getResourceAsStream(filename);

        if (stream == null) {
            logger.error("Cannot find resource file: {}", filename);
            return null;
        }

        return new BufferedReader(new InputStreamReader(stream));
    }
}
