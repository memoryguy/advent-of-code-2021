package online.wakkawakka;

import online.wakkawakka.util.Utility;
import online.wakkawakka.year.DayBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static online.wakkawakka.util.Utility.getDataVariants;

@SpringBootApplication
public class AdventOfCode2021 implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(AdventOfCode2021.class);
    private static final String ARG_YEAR = "year";
    private static final String ARG_DAY = "day";
    private static final String ARG_PART = "part";
    private static final String ARG_SAMPLE_DATA = "sampleData";
    private static final String ARG_VARIANT = "variant";
    private static final String ARG_LIST_VARIANTS = "listVariants";

    private int year = 2021;
    private int day = -1;
    private String part = "";
    private boolean sampleData = false;
    private String variant = "";
    private boolean listVariants = false;

    public static void main(String[] args) {
        SpringApplication.run(AdventOfCode2021.class, args);
    }

    private void parseArgs(ApplicationArguments args) throws IllegalStateException {
        Set<String> options = args.getOptionNames();

        if (options.contains(ARG_DAY) && options.contains(ARG_LIST_VARIANTS)) {
            this.day = Integer.parseInt(args.getOptionValues(ARG_DAY).get(0));
            this.listVariants = true;

            if (args.containsOption(ARG_YEAR)) {
                this.year = Integer.parseInt(args.getOptionValues(ARG_YEAR).get(0));
            }
        } else if (!(options.contains(ARG_DAY) && options.contains(ARG_PART))) {
            throw new IllegalStateException("Missing required arguments --day or --part");
        } else {
            this.day = Integer.parseInt(args.getOptionValues(ARG_DAY).get(0));
            this.part = args.getOptionValues(ARG_PART).get(0).toLowerCase();
            this.sampleData = args.containsOption(ARG_SAMPLE_DATA);

            if (!part.matches("^[ab]$")) {
                throw new IllegalStateException("Part must be 'a' or 'b'");
            }
        }

        if (args.containsOption(ARG_YEAR)) {
            this.year = Integer.parseInt(args.getOptionValues(ARG_YEAR).get(0));
        }

        if (args.containsOption(ARG_VARIANT)) {
            this.variant = args.getOptionValues(ARG_VARIANT).get(0);
        }

        if (day < 1 || day > 25) {
            throw new IllegalStateException("Day must be 1 to 25");
        }
    }

    private void displayUsageMessage() {
        logger.error("Usage: aoc2021 --day <day> --part <part> --sampleData --variant <variant>");
        logger.error("       aoc2021 --day <day> --listVariants");
        logger.error("Where:   <day >           day number to execute (1 to 25)");
        logger.error("         <part>           part to execute ('a' or 'b')");
        logger.error("         <variant>        variant of input data to use");
        logger.error("Options");
        logger.error("  --sampleData   use smaller SAMPLE data set, rather than TEST data");
        logger.error("  --variant      specify a VARIANT of test data, where available");
        logger.error("  --listVariants show all data variants for this day");
    }

    private void displayVariants() {
        List<List<String>> variants = Utility.getDataVariants(this.year, this.day);

        logger.info("{} Variants for Day {} SAMPLE data:", variants.get(0).size(), this.day);
        for (String v : variants.get(0)) {
            logger.info("  {}", v);
        }

        logger.info("{} Variants for Day {} TEST data:", variants.get(1).size(), this.day);
        for (String v : variants.get(1)) {
            logger.info("  {}", v);
        }
    }

    @Override
    public void run(ApplicationArguments args) {
        long solution;

        try {
            parseArgs(args);

            if (this.listVariants) {
                displayVariants();
            } else {
                DayBase c = (DayBase) Class.forName(String.format("online.wakkawakka.year.y%d.day.Day%02d",
                                                    year,
                                                    day)).getDeclaredConstructor(Integer.class,
                                                                                 Integer.class,
                                                                                 String.class,
                                                                                 Boolean.class,
                                                                                 String.class).newInstance(year,
                                                                                                           day,
                                                                                                           part,
                                                                                                           sampleData,
                                                                                                           variant);
                solution = c.runPart(part);

                logger.info("");
                if (solution != DayBase.NO_SOLUTION) {
                    logger.info("SOLUTION:");
                    logger.info("{}", solution);

                    Optional<Long> expectedResult = c.getExpectedResult();
                    if (expectedResult.isPresent()) {
                        if (expectedResult.get() == solution) {
                            logger.info("Result is correct");
                        } else {
                            logger.error("RESUlT IS NOT CORRECT (expecting {})", expectedResult.get());
                        }
                    }
                } else {
                    logger.error("===  no solution could be calculated  ===");
                }
            }
        } catch (NumberFormatException e) {
            logger.error("Not a number: {}", args.getNonOptionArgs().get(1));
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            logger.error("Day {} has not been implemented yet", day);
        } catch (IllegalStateException e) {
            displayUsageMessage();
            logger.error("ERROR: {}", e.getLocalizedMessage(), e);
        } catch (InvocationTargetException e) {
            Throwable ee = e.getCause();
            logger.error("Unable to run Day challenge:", ee);
        } catch (IllegalArgumentException e) {
            logger.error("Unable to parse input", e);
        }
    }
}
