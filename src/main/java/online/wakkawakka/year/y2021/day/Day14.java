package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Day14 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day14.class);

    private Map<String, List<String>> transformations;
    private String template;
    private boolean readingTemplate;

    public Day14(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.readingTemplate = true;
        this.transformations = new TreeMap<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        if (inputLine.isEmpty()) {
            readingTemplate = false;
        } else {
            if (readingTemplate) {
                this.template = inputLine;
            } else {
                String[] parts = inputLine.split("\\s*->\\s*");

                this.transformations.put(parts[0],
                        Arrays.asList(parts[0].charAt(0) + parts[1],
                                parts[1] + parts[0].charAt(1))
                );
            }
        }
    }

    private Map<Character, Long> getCharCounts(Map<String, Long> pairCounts) {
        Map<Character, Long> charCounts = new TreeMap<>();

        pairCounts.forEach((k, v) -> {
            char character = k.charAt(0);

            charCounts.put(character, charCounts.getOrDefault(character, 0L) + v);
        });

        return charCounts;
    }

    @Override
    public long partA() {
        Map<String, Long> counts = new TreeMap<>();
        Map<Character, Long> charCounts;

        // initialize the counts with the initial pairs from the template
        // string
        for (int i = 0; i < this.template.length()-1; i++) {
            String pair = this.template.substring(i, i+2);

            counts.put(pair, counts.getOrDefault(pair, 0L)+1);
        }

        for (int i = 0; i < 10; i++) {
            Map<String, Long> newCounts = new TreeMap<>();
            newCounts.put(this.template.substring(this.template.length()-1), 1L);

            for (Map.Entry<String, Long> entry : counts.entrySet()) {
                List<String> transform = this.transformations.get(entry.getKey());
                long count = entry.getValue();

                if (transform != null) {
                    logger.debug("{} becomes: {}", entry.getKey(), transform);

                    for (String t : transform) {
                        newCounts.put(t, newCounts.getOrDefault(t, 0L) + count);
                    }

                }
            }

            counts = newCounts;
        }

        charCounts = getCharCounts(counts);

        List<Long> vals = new ArrayList<>(charCounts.values());
        Collections.sort(vals);

        long highestCount = vals.get(vals.size() - 1);
        long lowestCount = vals.get(0);
        logger.debug("MAX: {}", highestCount);
        logger.debug("MIN: {}", lowestCount);

        if (highestCount < lowestCount) {
            logger.error("Something went wrong!");
        }

        return highestCount - lowestCount;
    }

    @Override
    public long partB() {
        Map<String, Long> counts = new TreeMap<>();

        // initialize the counts with the initial pairs from the template
        // string
        for (int i = 0; i < this.template.length()-1; i++) {
            String pair = this.template.substring(i, i+2);

            counts.put(pair, counts.getOrDefault(pair, 0L)+1);
        }

        for (int i = 0; i < 40; i++) {
            Map<String, Long> newCounts = new TreeMap<>();
            newCounts.put(this.template.substring(this.template.length()-1), 1L);

            for (Map.Entry<String, Long> entry : counts.entrySet()) {
                List<String> transform = this.transformations.get(entry.getKey());
                long count = entry.getValue();

                if (transform != null) {
                    logger.debug("{} becomes: {}", entry.getKey(), transform);

                    for (String t : transform) {
                        newCounts.put(t, newCounts.getOrDefault(t, 0L) + count);
                    }

                }
            }

            counts = newCounts;
        }

        List<Long> vals = new ArrayList<>(getCharCounts(counts).values());
        Collections.sort(vals);

        long highestCount = vals.get(vals.size() - 1);
        long lowestCount = vals.get(0);
        logger.debug("MAX: {}", highestCount);
        logger.debug("MIN: {}", lowestCount);

        if (highestCount < lowestCount) {
            logger.error("Something went wrong!");
        }

        return highestCount - lowestCount;
    }
}
