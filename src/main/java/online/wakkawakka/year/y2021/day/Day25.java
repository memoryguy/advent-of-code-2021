package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day25.Cucumber;
import online.wakkawakka.year.y2021.helper.day25.EastCucumber;
import online.wakkawakka.year.y2021.helper.day25.SouthCucumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Day25 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day25.class);
    private static final String MOVE_RIGHT = ">";
    private static final String MOVE_DOWN = "v";
    private static final String FREE_SPACE = ".";

    private List<Cucumber> map;
    private int sizeX;
    private int sizeY;

    public Day25(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.map = new ArrayList<>();
        this.sizeX = 0;
        this.sizeY = 0;
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        String[] chars = inputLine.split("");
        int x = 0;

        for (String c : chars) {
            if (c.equals(MOVE_RIGHT)) {
                this.map.add(new EastCucumber(x, this.sizeY));
            } else if (c.equals(MOVE_DOWN)) {
                this.map.add(new SouthCucumber(x, this.sizeY));
            }

            x++;
        }

        this.sizeY++;
        this.sizeX = inputLine.length();
    }

    private boolean positionFree(int x, int y) {
        return this.map.stream()
                .noneMatch(c -> c.getPosX() == x % this.sizeX
                        && c.getPosY() == y % this.sizeY);
    }

    private void displayMap() {
        if (!logger.isDebugEnabled()) return;

        for (int y = 0; y < this.sizeY; y++) {
            String line = "";

            for (int x = 0; x < this.sizeX; x++) {
                final int xx = x;
                final int yy = y;
                Optional<Cucumber> cucumber = this.map.stream().filter(c -> c.getPosX() == xx && c.getPosY() == yy).findFirst();
                if (cucumber.isPresent()) {
                    if (cucumber.get() instanceof EastCucumber) {
                        line += MOVE_RIGHT;
                    } else if (cucumber.get() instanceof SouthCucumber) {
                        line += MOVE_DOWN;
                    } else {
                        line += "X";
                    }
                } else {
                    line += FREE_SPACE;
                }
            }

            logger.debug(line);
        }
    }

    private int moveCucumbers() {
        final List<Cucumber> toMove = new ArrayList<>();
        int moves = 0;

        // move only the EAST cucumbers first
        this.map.stream()
                .filter(EastCucumber.class::isInstance)
                .filter(c -> positionFree(c.getPosX()+1, c.getPosY()))
                .forEach(toMove::add);

        logger.debug("Found {} cucumbers to move EAST: {}", toMove.size(), toMove);
        moves += toMove.size();

        toMove.forEach(c -> c.incPosX(this.sizeX));
        toMove.clear();

        // move the SOUTH cucumbers AFTER the east
        this.map.stream()
                .filter(SouthCucumber.class::isInstance)
                .filter(c -> positionFree(c.getPosX(), c.getPosY()+1))
                .forEach(toMove::add);

        logger.debug("Found {} cucumbers to move SOUTH: {}", toMove.size(), toMove);
        moves += toMove.size();

        toMove.forEach(c -> c.incPosY(this.sizeY));

        return moves;
    }

    @Override
    public long partA() {
        int move = 1;

        while (moveCucumbers() != 0) {
            displayMap();
            move++;
        }

        return move;
    }

    @Override
    public long partB() {
        return super.partB();
    }
}
