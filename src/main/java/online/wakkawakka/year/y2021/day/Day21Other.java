package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day21.UniverseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCacheManager;

import java.io.IOException;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class Day21Other extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day21Other.class);
    private static final int WINNING_SCORE = 21;
    private static final String CACHE_NAME = "diracDice";

    enum CacheMode {
        NONE, HASH, CAFFEINE, REDIS
    }
    private CacheMode cacheMode;

    private CacheManager cacheManager = new CaffeineCacheManager(CACHE_NAME);
    private final Map<String, UniverseResult> UNIVERSE_RESULTS = new HashMap<>();

    public Day21Other(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    public long partB() {
        int p1Position = Integer.parseInt(this.inputLines.get(0).split("starting position: ")[1]);
        int p2Position = Integer.parseInt(this.inputLines.get(1).split("starting position: ")[1]);

        Instant hashStart = Instant.now();
        this.cacheMode = CacheMode.HASH;
        this.cacheManager = new CaffeineCacheManager(CACHE_NAME);
        UniverseResult result = playGame(0, 0, p1Position, p2Position, 0, 0);
        Instant hashEnd = Instant.now();

        logger.info("Hash P1: {}", result.getP1Wins());
        logger.info("Hash P2: {}", result.getP2Wins());

        Instant caffeineStart = Instant.now();
        this.cacheMode = CacheMode.CAFFEINE;
        this.cacheManager = new CaffeineCacheManager(CACHE_NAME);
        result = playGame(0, 0, p1Position, p2Position, 0, 0);
        Instant caffeineEnd = Instant.now();

        logger.info("Caffeine P1: {}", result.getP1Wins());
        logger.info("Caffeine P2: {}", result.getP2Wins());

//        Instant nopStart = Instant.now();
//        this.cacheMode = CacheMode.NONE;
//        this.cacheManager = new NoOpCacheManager();
//        result = playGame(0, 0, p1Position, p2Position, 0, 0, "none");
//        Instant nopEnd = Instant.now();
//
//        logger.info("Uncached P1: {}", result.getP1Wins());
//        logger.info("Uncached P2: {}", result.getP2Wins());

        logger.info("Hash duration: {}", Duration.between(hashStart, hashEnd).toMillis());
        logger.info("Caffeine duration: {}", Duration.between(caffeineStart, caffeineEnd).toMillis());
//        logger.info("Uncached duration: {}", Duration.between(nopStart, nopEnd).toMillis());

        return Math.max(result.getP1Wins().longValue(), result.getP2Wins().longValue());
    }

    private int endPosition(int position, int diceRoll) {
        return ((position-1 + diceRoll) % 10) + 1;
    }

    private UniverseResult getCachedResult(String cacheKey) {
        if (this.cacheMode == CacheMode.HASH) {
            return UNIVERSE_RESULTS.getOrDefault(cacheKey, null);
        } else {
            Cache cache = this.cacheManager.getCache(CACHE_NAME);
            if (cache != null) {
                Cache.ValueWrapper previousResult = cache.get(cacheKey);

                if (previousResult != null) {
                    return (UniverseResult) previousResult.get();
                }
            }
        }

        return null;
    }

    private void saveCachedResult(String cacheKey, UniverseResult result) {
        if (this.cacheMode == CacheMode.HASH) {
            UNIVERSE_RESULTS.put(cacheKey, result);
        } else {
            Cache cache = this.cacheManager.getCache(CACHE_NAME);
            if (cache != null) {
                cache.put(cacheKey, result);
            }
        }
    }

    public UniverseResult playGame(int p1Score, int p2Score, int p1Position, int p2Position, int turn, int throwSum) {
        boolean isP1Turn = turn < 3;
        boolean lastP1Throw = turn == 2;
        boolean lastP2Throw = turn == 5;

        String cacheKey = String.format("%d-%d-%d-%d-%d-%d", p1Position, p1Score, p2Position, p2Score, turn, throwSum);

        UniverseResult res;

        if (p1Score >= WINNING_SCORE) {
            res = new UniverseResult(cacheKey, BigInteger.ONE, BigInteger.ZERO);
        } else if (p2Score >= WINNING_SCORE) {
            res = new UniverseResult(cacheKey, BigInteger.ZERO, BigInteger.ONE);
        } else {
            UniverseResult cachedResult = getCachedResult(cacheKey);
            if (cachedResult != null) {
                return cachedResult;
            }

            int nextTurn = (turn+1) % 6;
            BigInteger p1Wins = BigInteger.ZERO;
            BigInteger p2Wins = BigInteger.ZERO;

            for (int roll = 1; roll <= 3; roll++) {
                UniverseResult result = playGame(
                        isP1Turn ? p1Score + (lastP1Throw ? endPosition(p1Position, roll) : 0) : p1Score,
                        !isP1Turn ? p2Score + (lastP2Throw ? endPosition(p2Position, roll) : 0) : p2Score,
                        isP1Turn ? endPosition(p1Position, roll) : p1Position,
                        !isP1Turn ? endPosition(p2Position, roll) : p2Position,
                        nextTurn,
                        turn % 3 == 0 ? 0 : throwSum + roll
                );

                p1Wins = p1Wins.add(result.getP1Wins());
                p2Wins = p2Wins.add(result.getP2Wins());
            }

            res = new UniverseResult(cacheKey, p1Wins, p2Wins);
        }

        saveCachedResult(cacheKey, res);
        return res;
    }
}
