package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day08.Segments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day08 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day08.class);

    private List<List<Segments>> observations;
    private List<List<Segments>> values;

    public Day08(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.observations = new ArrayList<>();
        this.values = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        String[] parts = inputLine.split("\\s*\\|\\s*");

        List<Segments> oneObservation = Arrays.stream(parts[0].split("\\s+")).map(Segments::new).collect(Collectors.toList());
        List<Segments> oneValue = Arrays.stream(parts[1].split("\\s+")).map(Segments::new).collect(Collectors.toList());

        observations.add(oneObservation);
        values.add(oneValue);
    }

    @Override
    public long partA() {
        return values.stream()
                .mapToLong(v -> v.stream()
                        .filter(s -> s.size() == 2 || s.size() == 3 || s.size() == 4 || s.size() == 7)
                        .count())
                .sum();
    }

    // This solution is heavily inspired by Daniel Persson's solution at
    //    https://github.com/kalaspuffar/advent2021/blob/main/src/main/src/main/java/Day8b.java
    // and explained in
    //    https://www.youtube.com/watch?v=LIBd-Q-n8JE
    @Override
    public long partB() {
        long value = 0;

        for (int i = 0; i < observations.size(); i++) {
            Map<Segments, String> numbers = decipherDigits(observations.get(i));

            // sanity check; if we were not able to map 10 digits somehow,
            // then there is a bug somewhere...
            if (numbers.size() < 10) {
                logger.error("Numbers map is too short! ({}}", numbers.size());

                for (int j = 0; j < 10; j++) {
                    if (!numbers.containsValue(String.valueOf(j))) {
                        logger.error("  -> missing {}", j);
                    }
                }
            }

            // map each `value` to its corresponding digit value
            // (character/string), join the characters together into a
            // string and then convert to an integer/long. Add that to the
            // running total
            value += Long.parseLong(values.get(i).stream()
                    .map(numbers::get)
                    .collect(Collectors.joining()));
        }

        return value;
    }

    private Map<Segments, String> decipherDigits(List<Segments> values) {
        Map<Segments, String> numbers = new HashMap<>();
        Map<String, Segments> numbersReverse = new HashMap<>();

        List<Segments> fiveSegments = new ArrayList<>();
        List<Segments> sixSegments = new ArrayList<>();

        for (Segments num : values) {
            if (num.getValue() != -1) {
                numbers.put(num, String.valueOf(num.getValue()));
                numbersReverse.put(String.valueOf(num.getValue()), num);
            } else if (num.size() == 5) {
                fiveSegments.add(num);
            } else if (num.size() == 6) {
                sixSegments.add(num);
            }
        }

        Segments segmentA = numbersReverse.get("7")
                .subtract(numbersReverse.get("1"))
                .add(numbersReverse.get("4"));

        Segments nine = segmentA.mostSimilarTo(sixSegments);
        logger.debug("Nine is most similar to {}", nine);
        numbers.put(nine, "9");
        numbersReverse.put("9", nine);
        sixSegments.remove(nine);

        Segments five = nine.subtract(numbersReverse.get("1")).mostSimilarTo(fiveSegments);
        numbers.put(five, "5");
        numbersReverse.put("5", five);
        fiveSegments.remove(five);

        Segments three = five.subtract(numbersReverse.get("4"))
                .add(numbersReverse.get("1").mostSimilarTo(fiveSegments));
        numbers.put(three, "3");
        numbersReverse.put("3", three);
        fiveSegments.remove(three);

        Segments two = fiveSegments.get(0);
        numbers.put(two, "2");
        numbersReverse.put("2", two);
        fiveSegments.remove(two);

        Segments zero = two.subtract(numbersReverse.get("4"))
                .add(numbersReverse.get("1"))
                .mostSimilarTo(sixSegments);
        numbers.put(zero, "0");
        numbersReverse.put("0", zero);
        sixSegments.remove(zero);

        Segments six = sixSegments.get(0);
        numbers.put(six, "6");
        numbersReverse.put("6", six);
        sixSegments.remove(six);

        return numbers;
    }
}
