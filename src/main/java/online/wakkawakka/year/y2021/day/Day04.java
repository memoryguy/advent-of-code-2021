package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day04.BingoCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day04 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day04.class);

    private final List<String> tempBoard = new ArrayList<>();
    private final List<BingoCard> boards = new ArrayList<>();
    private List<Integer> numbers;

    public Day04(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        if (numbers.isEmpty()) {
            // the first line of the file is the set of numbers to be called,
            // comma-separated. Split on the comma, convert to integer, and
            // save in a List
            numbers = Arrays.stream(inputLine.split(",")).map(Integer::parseInt).collect(Collectors.toList());
        } else {
            // if the line is empty, it means we've finished reading a
            // card. If there are lines in the card, then parse them into a
            // BingoCard and add it to the list of boards. If the line is
            // not empty then it's part of the card we're reading; add it
            // to the list of lines for this card
            if (inputLine.isEmpty()) {
                if (!tempBoard.isEmpty()) {
                    // end of board definition; build it
                    boards.add(new BingoCard(tempBoard));

                    tempBoard.clear();
                }
            } else {
                tempBoard.add(inputLine);
            }
        }
    }

    @Override
    public long partA() {
        // call the numbers and look for a winner
        for (int number : numbers) {
            for (BingoCard board : boards) {
                board.markNumber(number);

                if (board.isWinner()) {
                    return (long) board.sumAllUnmarked()*number;
                }
            }
        }

        return super.partA();
    }

    @Override
    public long partB() {
        // call the numbers and look for the last winner. This will be the
        // first moment that ALL cards have won
        for (int number : numbers) {
            for (BingoCard board : boards) {
                board.markNumber(number);

                if (boards.stream().allMatch(BingoCard::isWinner)) {
                    return (long) board.sumAllUnmarked()*number;
                }
            }
        }

        return super.partB();
    }
}
