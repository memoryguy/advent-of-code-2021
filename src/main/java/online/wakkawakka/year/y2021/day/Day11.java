package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day11.Octopus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day11 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day11.class);

    private List<List<Octopus>> octopi;
    private final int rowCount;
    private final int columnCount;
    private int step = 1;

    public Day11(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);

        rowCount = octopi.size();
        columnCount = octopi.get(0).size();
    }

    @Override
    protected void initializeMembers() {
        this.octopi = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        octopi.add(Arrays.stream(inputLine.trim().split(""))
                .map(o -> new Octopus(Integer.parseInt(o)))
                .collect(Collectors.toList()));
    }

    private void increasePower(int row, int col) {
        if (row < 0 || row >= this.rowCount || col < 0 || col >= this.columnCount) return;

        octopi.get(row).get(col).increasePowerLevel();
    }

    private void flashOctopus(int row, int col) {
        if (row < 0 || row >= this.rowCount || col < 0 || col >= this.columnCount) return;

        if (octopi.get(row).get(col).canFlash()) {
            for (int r = -1; r < 2; r++) {
                for (int c = -1; c < 2; c++) {
                    increasePower(row+r, col+c);
                    flashOctopus(row+r, col+c);
                }
            }
        }
    }

    @Override
    public long partA() {
        while (step <= 100) {
            // first, increase everyone's power level
            octopi.forEach(r -> r.forEach(Octopus::increasePowerLevel));

            // now find all the surrounding octopi that can flash,
            for (int row = 0; row < this.rowCount; row++) {
                for (int col = 0; col < this.columnCount; col++) {
                    flashOctopus(row, col);
                }
            }

            // reset all the flashes
            octopi.forEach(r -> r.forEach(Octopus::resetFlash));

            step++;
        }

        return octopi.stream().mapToInt(r -> r.stream().mapToInt(Octopus::getFlashCount).sum()).sum();
    }

    @Override
    public long partB() {
        while (true) {
            // first, increase everyone's power level
            octopi.forEach(r -> r.forEach(Octopus::increasePowerLevel));

            // now find all the surrounding octopi that can flash,
            for (int row = 0; row < this.rowCount; row++) {
                for (int col = 0; col < this.columnCount; col++) {
                    flashOctopus(row, col);
                }
            }

            // check for all octopuseseses to have flashed in this
            // iteration: find the rows that have all columns flashed. If
            // all the rows match, it means everyone went off at once
            if (octopi.stream().filter(r -> r.stream().allMatch(Octopus::isFlashed)).count() == this.rowCount) {
                break;
            }

            // reset all the flashes
            octopi.forEach(r -> r.forEach(Octopus::resetFlash));

            step++;
        }

        return step;
    }
}
