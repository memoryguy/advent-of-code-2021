package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Day02 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day02.class);

    private long position = 0;
    private long depth = 0;
    private long aim = 0;

    public Day02(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    public long partA() {
        try {
            for (String inputLine : inputLines) {
                String[] fields = inputLine.split("\\s+");
                String command = fields[0];
                long amount = Long.parseLong(fields[1]);

                switch (command.toLowerCase()) {
                    case "forward":
                        position += amount;
                        break;
                    case "down":
                        depth += amount;
                        break;
                    case "up":
                        depth -= amount;
                        break;
                    default:
                        logger.error("Unknown command: [{}]", command);
                }
            }

            logger.debug("Final position: {}, depth: {}", position, depth);

            return position*depth;
        } catch (NumberFormatException e) {
            logger.error("Not a recognized number format on line");
        }

        return super.partA();
    }

    @Override
    public long partB() {
        try {
            for (String inputLine : inputLines) {
                String[] fields = inputLine.split("\\s+");
                String command = fields[0];
                long amount = Long.parseLong(fields[1]);

                switch (command.toLowerCase()) {
                    case "forward":
                        position += amount;
                        depth += aim*amount;
                        break;
                    case "down":
                        aim += amount;
                        break;
                    case "up":
                        aim -= amount;
                        break;
                    default:
                        logger.error("Unknown command: [{}]", command);
                }
            }

            logger.debug("Final position: {}, depth: {}", position, depth);

            return position*depth;
        } catch (NumberFormatException e) {
            logger.error("Not a recognized number format on line");
        }

        return super.partB();
    }
}
