package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day06.LanternFish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day06 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day06.class);

    private List<LanternFish> fish;
    private long[] population;

    public Day06(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.fish = new ArrayList<>();
        this.population = new long[9];
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        // load for Part A
        fish.addAll(Arrays.stream(inputLine.trim().split(","))
                .map(age -> new LanternFish(Byte.parseByte(age)))
                .collect(Collectors.toList()));

        // load for Part B
        Arrays.asList(inputLine.trim().split(","))
                .forEach(age -> population[Integer.parseInt(age)]++);
    }

    @Override
    public long partA() {
        int day;

        logger.debug("Initial state: {}", fish);

        for (day = 0; day < 80; day++) {
            int newFishCount = 0;

            for (LanternFish oneFish : fish) {
                if (oneFish.getCounter() == 0) {
                    oneFish.setCounter((byte) 6);
                    newFishCount++;
                } else {
                    oneFish.decrement();
                }
            }

            for (int i = 0; i < newFishCount; i++) {
                fish.add(new LanternFish());
            }

            logger.debug("After {} days: ({}) {}", day+1, fish.size(), fish);
        }

        return fish.size();
    }

    @Override
    public long partB() {
        int day;

        logger.debug("Initial state: {}", population);

        for (day = 0; day < 256; day++) {
            long tmpPopZero = population[0];

            System.arraycopy(population, 1, population, 0, 6);

            population[6] = tmpPopZero+population[7];
            population[7] = population[8];
            population[8] = tmpPopZero;

            logger.debug("After {} days: {}  -> {} ", day+1, totalPopulation(), population);
        }

        return totalPopulation();
    }

    private long totalPopulation() {
        return Arrays.stream(population).reduce(0L, Long::sum);
    }
}
