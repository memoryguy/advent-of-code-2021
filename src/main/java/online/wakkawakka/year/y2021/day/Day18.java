package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day18.SnailPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Day18 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day18.class);

    private List<SnailPair> snailPairs;

    public Day18(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.snailPairs = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        snailPairs.add(new SnailPair(inputLine.trim(), 1));
        logger.debug("Added number: {}", snailPairs.get(snailPairs.size() - 1));
    }

    private void reduceResult(SnailPair snail) {

    }

    private SnailPair addNumbers() {
        SnailPair result = snailPairs.remove(0);

        while (!snailPairs.isEmpty()) {
            result = new SnailPair(result, snailPairs.remove(0), 1);
        }

        result.reduce();

        return result;
    }

    @Override
    public long partA() {
        SnailPair result = addNumbers();
        logger.debug("Add result: {}", result);

        return result.getMagnitude();
    }

    @Override
    public long partB() {
        return super.partB();
    }
}
