package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day13.Fold;
import online.wakkawakka.year.y2021.helper.day13.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static online.wakkawakka.year.y2021.helper.day13.Fold.Direction.FOLD_LEFT;
import static online.wakkawakka.year.y2021.helper.day13.Fold.Direction.FOLD_UP;

public class Day13 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day13.class);

    private List<Point> points;
    private List<Fold> folds;
    private boolean buildReadingPoints;

    public Day13(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.buildReadingPoints = true;
        this.points = new ArrayList<>();
        this.folds = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        inputLine = inputLine.trim();

        if (inputLine.isEmpty()) {
            buildReadingPoints = false;
        } else {
            if (buildReadingPoints) {
                Point newPoint = new Point(inputLine);

                points.add(newPoint);
            } else {
                folds.add(new Fold(inputLine));
            }
        }
    }

    private void foldLine(Fold fold) {
        List<Point> pointsToRemove = new ArrayList<>();

        points.forEach(p -> {
            int newX = p.getX();
            int newY = p.getY();
            boolean pointMoved = false;

            // if the point is on the side being folded, reposition it to its
            // landing position
            if (fold.getDirection() == FOLD_UP && p.getY() > fold.getFoldLine()) {
                newY = fold.getFoldLine() - (p.getY() - fold.getFoldLine());
                pointMoved = true;
            } else if (fold.getDirection() == FOLD_LEFT && p.getX() > fold.getFoldLine()) {
                newX = fold.getFoldLine() - (p.getX() - fold.getFoldLine());
                pointMoved = true;
            }

            if (pointMoved && points.contains(new Point(newX, newY))) {
                // this overlaps an existing point; we should remove the
                // original point, but we can't do it during the loop, so we'll
                // do it at the end
                pointsToRemove.add(p);
            } else {
                // it's not an overlapping point, so save its updated position
                p.setX(newX);
                p.setY(newY);
            }
        });

        // remove all the overlapping points we found
        points.removeAll(pointsToRemove);
    }

    @Override
    public long partA() {
        foldLine(folds.get(0));

        return points.size();
    }

    private List<String> visualizePoints() {
        int dimensionX = points.stream().mapToInt(Point::getX).max().orElseThrow(IllegalStateException::new)+1;
        int dimensionY = points.stream().mapToInt(Point::getY).max().orElseThrow(IllegalStateException::new)+1;
        List<String> output = new ArrayList<>();
        StringBuilder line = new StringBuilder();

        for (int y = 0; y < dimensionY; y++) {
            line.setLength(0);

            for (int x = 0; x < dimensionX; x++) {
                line.append(points.contains(new Point(x, y)) ? "X" : ".");
            }

            output.add(line.toString());
        }

        return output;
    }

    @Override
    public long partB() {
        for (Fold fold : folds) {
            foldLine(fold);
        }

        visualizePoints().forEach(logger::info);

        return points.size();
    }
}
