package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day17 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day17.class);
    private static final int RANGE_LOW = -1000;
    private static final int RANGE_HIGH = 1000;
    private static final int RANGE = Math.abs(RANGE_LOW) + Math.abs(RANGE_HIGH);
    private static final Pattern regex = Pattern.compile("^target area: x=(?<xLow>[\\d\\-]+)\\.\\.(?<xHigh>[\\d\\-]+), y=(?<yLow>[\\d\\-]+)\\.\\.(?<yHigh>[\\d\\-]+)$");

    private int targetLowX;
    private int targetHighX;
    private int targetLowY;
    private int targetHighY;
    private final long[][] solutions = new long[RANGE][RANGE];

    public Day17(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        Matcher m = regex.matcher(inputLine.trim());

        if (m.find()) {
            this.targetLowX = Integer.parseInt(m.group("xLow"));
            this.targetHighX = Integer.parseInt(m.group("xHigh"));
            this.targetLowY = Integer.parseInt(m.group("yLow"));
            this.targetHighY = Integer.parseInt(m.group("yHigh"));
        } else {
            throw new IllegalArgumentException("Unable to parse input line: " + inputLine);
        }
    }

    private void populateTable() {
        logger.debug("Calculating all solutions for initial velocities {} to {}...", RANGE_LOW, RANGE_HIGH);

        Instant start = Instant.now();

        // Dynamic Programming solution: calculate ALL of the solutions
        // between initial velocities -1000 to 1000. Then we can just look
        // through the table to find the max height, or the total number of
        // valid solutions

        for (int yInitialVel = RANGE_LOW; yInitialVel < RANGE_HIGH; yInitialVel++) {
            for (int xInitialVel = RANGE_LOW; xInitialVel < RANGE_HIGH; xInitialVel++) {
                int xvel = xInitialVel;
                int yvel = yInitialVel;
                int xpos = 0;
                int ypos = 0;
                int highY = 0;

                this.solutions[yInitialVel+RANGE_HIGH][xInitialVel+RANGE_HIGH] = Integer.MIN_VALUE;

                while (ypos >= this.targetLowY) {
                    xpos += xvel;
                    ypos += yvel;

                    if (ypos > highY) {
                        highY = ypos;
                    }

                    xvel = xvel + (Integer.compare(0, xvel));
                    yvel--;

                    if (xpos >= this.targetLowX && xpos <= this.targetHighX && ypos >= this.targetLowY && ypos <= this.targetHighY) {
                        this.solutions[yInitialVel+RANGE_HIGH][xInitialVel+RANGE_HIGH] = highY;
                        break;
                    }
                }
            }
        }

        Instant end = Instant.now();
        logger.debug("Calculation complete in {}", Duration.between(start, end));
    }

    @Override
    public long partA() {
        populateTable();

        // look through the table of solutions and find the max value
        int bestX = 0;
        int bestY = 0;
        long bestHeight = 0;
        for (int y = 0; y < RANGE; y++) {
            for (int x = 0; x < RANGE; x++) {
                if (this.solutions[y][x] > bestHeight) {
                    bestHeight = this.solutions[y][x];
                    bestX = x+RANGE_LOW;
                    bestY = y+RANGE_LOW;
                }
            }
        }

        logger.debug("Best height is {}   at position ({}, {})", bestHeight, bestX, bestY);
        return bestHeight;
    }

    @Override
    public long partB() {
        populateTable();

        // look through the table and count all the valid solutions
        int validCount = 0;
        for (int y = 0; y < RANGE; y++) {
            for (int x = 0; x < RANGE; x++) {
                if (this.solutions[y][x] != Integer.MIN_VALUE) {
                    logger.debug("Found valid solution: ({},{})", y+RANGE_LOW, x+RANGE_LOW);
                    validCount++;
                }
            }
        }

        return validCount;
    }
}
