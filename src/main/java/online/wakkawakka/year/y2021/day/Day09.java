package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day09.Basin;
import online.wakkawakka.year.y2021.helper.day09.FloorPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day09 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day09.class);

    private List<List<FloorPoint>> floor;
    private int buildRow = 0;

    public Day09(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.floor = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        int column = 0;
        List<FloorPoint> line = new ArrayList<>();

        for (String depth : inputLine.trim().split("")) {
            line.add(new FloorPoint(buildRow, column++, Integer.parseInt(depth)));
        }

        floor.add(line);
        buildRow++;
    }

    private List<FloorPoint> findLowPoints() {
        List<FloorPoint> lowPoints = new ArrayList<>();

        for (int row = 0; row < this.floor.size(); row++) {
            for (int column = 0; column < this.floor.get(row).size(); column++) {
                int curValue = this.floor.get(row).get(column).getDepth();

                // check above, below, left, and right. If any of those
                // values are SMALLER than the current value, then skip to
                // the next iteration
                if ((row > 0 && this.floor.get(row-1).get(column).getDepth() <= curValue) ||
                        (row < this.floor.size()-1 && this.floor.get(row+1).get(column).getDepth() <= curValue) ||
                        (column > 0 && this.floor.get(row).get(column-1).getDepth() <= curValue) ||
                        (column < this.floor.get(row).size()-1 && this.floor.get(row).get(column+1).getDepth() <= curValue)) {
                    continue;
                }

                lowPoints.add(new FloorPoint(row, column, curValue));
            }
        }

        return lowPoints;
    }

    @Override
    public long partA() {
        List<FloorPoint> lowPoints = findLowPoints();

        return lowPoints.stream().mapToInt(v -> v.getDepth()+1).sum();
    }

    @Override
    public long partB() {
        List<Basin> basins = new ArrayList<>();
        List<FloorPoint> lowPoints = findLowPoints();

        for (FloorPoint point : lowPoints) {
            Basin basin = new Basin();
            basins.add(findBasinForPoint(point, basin));
        }

        logger.debug("# of Low Points: {}", lowPoints.size());
        logger.debug("# of Basins: {}", basins.size());
        logger.debug("Basin sizes: {}", basins.stream().map(ArrayList::size).collect(Collectors.toList()));

        if (basins.size() < 3) {
            logger.error("FEWER THAN 3 BASINS WERE FOUND. THIS IS WRONG!");
        }

        // sort the basins from LARGEST to SMALLEST (reverse order), and
        // multiply the first three together. This will be a problem if
        // there are fewer than three basins found, which is what the
        // check/error message above will detect
        basins.sort((a, b) -> b.size() - a.size());
        return (long) basins.get(0).size()
                * basins.get(1).size()
                * basins.get(2).size();
    }

    private Basin findBasinForPoint(FloorPoint point, Basin basin) {
        if (point.getDepth() == 9 || point.isInBasin()) {
            return basin;
        }

        basin.add(point);
        this.floor.get(point.getRow()).get(point.getColumn()).setInBasin(true);

        // check above
        if (point.getRow() > 0
                && this.floor.get(point.getRow()-1).get(point.getColumn()).getDepth() > point.getDepth()) {
            findBasinForPoint(this.floor.get(point.getRow()-1).get(point.getColumn()), basin);
        }

        // check below
        if (point.getRow() < this.floor.size()-1
                && this.floor.get(point.getRow()+1).get(point.getColumn()).getDepth() > point.getDepth()) {
            findBasinForPoint(this.floor.get(point.getRow()+1).get(point.getColumn()), basin);
        }

        //check left <-
        if (point.getColumn() > 0
                && this.floor.get(point.getRow()).get(point.getColumn()-1).getDepth() > point.getDepth()) {
            findBasinForPoint(this.floor.get(point.getRow()).get(point.getColumn()-1), basin);
        }

        //check right ->
        if (point.getColumn() < this.floor.get(point.getRow()).size()-1
                && this.floor.get(point.getRow()).get(point.getColumn()+1).getDepth() > point.getDepth()) {
            findBasinForPoint(this.floor.get(point.getRow()).get(point.getColumn()+1), basin);
        }

        return basin;
    }
}
