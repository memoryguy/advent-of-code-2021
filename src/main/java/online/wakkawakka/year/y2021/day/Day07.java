package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class Day07 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day07.class);

    private List<Integer> positions;
    private int bestPosition = -1;
    private long bestFuel = Long.MAX_VALUE;
    private int maxPosition;

    public Day07(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);

        maxPosition = positions.stream().mapToInt(v -> v).max().orElseThrow(NoSuchElementException::new);
    }

    @Override
    protected void initializeMembers() {
        this.positions = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        positions.addAll(Arrays.stream(inputLine.trim().split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList()));
    }

    @Override
    public long partA() {
        for (int position = 0; position < maxPosition; position++) {
            final int pp = position;
            long fuelUsed = positions.stream().mapToLong(p -> Math.abs(p - pp)).reduce(0L, Long::sum);

            if (fuelUsed < this.bestFuel) {
                this.bestFuel = fuelUsed;
                this.bestPosition = position;
            }

            logger.debug("Pos {}, fuelUsed: {} (best pos: {}, best fuel: {}", position, fuelUsed, this.bestPosition, this.bestFuel);
        }

        return this.bestFuel;
    }

    @Override
    public long partB() {
        for (int position = 0; position < maxPosition; position++) {
            final int pp = position;
            long fuelUsed = positions.stream().mapToLong(p -> {
                long movement = Math.abs(p - pp);
                return (movement * (movement+1))/2;
            }).reduce(0L, Long::sum);

            if (fuelUsed < this.bestFuel) {
                this.bestFuel = fuelUsed;
                this.bestPosition = position;
            }

            logger.debug("Pos {}, fuelUsed: {} (best pos: {}, best fuel: {}", position, fuelUsed, this.bestPosition, this.bestFuel);
        }

        return this.bestFuel;
    }
}
