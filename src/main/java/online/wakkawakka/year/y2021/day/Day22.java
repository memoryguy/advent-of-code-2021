package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day22.Instruction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Day22 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day22.class);

    private List<Instruction> instructions;
    private final Map<String, Boolean> reactor = new TreeMap<>();

    public Day22(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.instructions = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        instructions.add(new Instruction(inputLine));
    }

    private void setReactorState(Instruction instruction) {
        for (int z = instruction.getZLow(); z <= instruction.getZHigh(); z++) {
            for (int y = instruction.getYLow(); y <= instruction.getYHigh(); y++) {
                for (int x = instruction.getXLow(); x <= instruction.getXHigh(); x++) {
                    this.reactor.put(x+","+y+","+z, instruction.isOn());
                }
            }
        }
    }

    @Override
    public long partA() {
        for (Instruction instruction : this.instructions) {
            if (instruction.inRangeA()) {
                logger.debug("Processing: {}", instruction);
                setReactorState(instruction);
            }
        }

        return this.reactor.values().stream().filter(v -> v).count();
    }

    @Override
    public long partB() {
        for (Instruction instruction : this.instructions) {
            logger.debug("Processing: {}", instruction);
            setReactorState(instruction);
        }

        return this.reactor.values().stream().filter(v -> v).count();
    }
}
