package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day05.Coordinate;
import online.wakkawakka.year.y2021.helper.day05.Line;
import online.wakkawakka.year.y2021.helper.day05.OceanFloor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Day05 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day05.class);
    
    private int maxX;
    private int maxY;
    private final OceanFloor oceanFloor;
    private List<Line> lines;

    public Day05(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);

        logger.debug("Floor size: (0,0) -> ({},{})", this.maxY, this.maxX);
        this.oceanFloor = new OceanFloor(this.maxX+1, this.maxY+1);
    }

    @Override
    protected void initializeMembers() {
        this.lines = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        String[] parts = inputLine.split("\\s*->\\s*");

        Coordinate start = new Coordinate(parts[0]);
        Coordinate end = new Coordinate(parts[1]);

        // track the maximum size
        if (start.getX() > this.maxX) this.maxX = start.getX();
        if (end.getX() > this.maxX) this.maxX = end.getX();
        if (start.getY() > this.maxY) this.maxY = start.getY();
        if (end.getY() > this.maxY) this.maxY = end.getY();

        lines.add(new Line(start, end));
    }

    @Override
    public long partA() {
        // for part A we only have to mark horizontal and vertical lines
        lines.stream()
                .filter(l -> l.getStart().getX() == l.getEnd().getX() || l.getStart().getY() == l.getEnd().getY())
                .forEach(oceanFloor::markLine);

        return oceanFloor.overlapPointCount();
    }

    @Override
    public long partB() {
        lines.forEach(oceanFloor::markLine);

        return oceanFloor.overlapPointCount();
    }
}
