package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day15.Coordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Day15 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day15.class);

    private List<List<Integer>> map;
    List<Coordinate> path = new ArrayList<>();
    private long currentBestRisk = Long.MAX_VALUE;

    public Day15(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.map = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        map.add(Arrays.stream(inputLine.trim().split(""))
                .map(Integer::parseInt)
                .collect(Collectors.toList()));
    }

    private long getPathRisk(List<Coordinate> path) {
        return path.stream().mapToLong(p -> this.map.get(p.getRow()).get(p.getColumn())).sum();
    }

    private List<Coordinate> traverseCave(int row, int col, List<Coordinate> pathToHere) {
        Coordinate here = new Coordinate(row, col);
        List<Coordinate> newPath = new ArrayList<>(pathToHere);

        long minPathWeight = Long.MAX_VALUE;
        List<Coordinate> bestPath = Collections.emptyList();

        logger.debug("Checking coordinate ({}, {})", col, row);
        logger.debug("pathToHere ({}) -> {}", pathToHere.size(), pathToHere.stream().map(p -> this.map.get(p.getRow()).get(p.getColumn())).collect(Collectors.toList()));

        //   o if we have visited this position before, we have a loop. Stop
        //     searching this path, by returning MAX_VALUE
        //   o if we've reach the target position, return the current path
        //     value
        //   o if we've gone outside the range of the grid, stop searching this
        //     path, return MAX_VALUE
        if (pathToHere.contains(here)) {
            logger.debug("Duplicate coordinate found, this path is not valid");
            return Collections.emptyList();
        } else if (row == 0 && col == 0) {
            logger.debug("GOT TO THE END! stop right here :)");
            return newPath;
        } else if (row < 0 || row > this.map.size()-1 || col < 0 || col > this.map.get(0).size()-1) {
            logger.debug("Went out of range, this path is not valid");
            return Collections.emptyList();
        } else if (getPathRisk(pathToHere) > this.currentBestRisk) {
            return Collections.emptyList();
        }

        // add this position to the path for sub-searching, to detect loops
        newPath.add(here);

        for (int rowOffset = -1; rowOffset <= 1; rowOffset += 2) {
            // find the lowest weight path in each direction from here
            List<Coordinate> subPath = traverseCave(row+rowOffset, col, newPath);
            if (!subPath.isEmpty()) {
                long tmpWeight = getPathRisk(subPath);
                if (tmpWeight < minPathWeight) {
                    minPathWeight = tmpWeight;
                    bestPath = subPath;
                }
            }
        }
        for (int colOffset = -1; colOffset <= 1; colOffset += 2) {
            // find the lowest weight path in each direction from here
            List<Coordinate> subPath = traverseCave(row, col+colOffset, newPath);
            if (!subPath.isEmpty()) {
                long tmpWeight = getPathRisk(subPath);
                if (tmpWeight < minPathWeight) {
                    minPathWeight = tmpWeight;
                    bestPath = subPath;
                }
            }
        }

        // if we still have MAX_VALUE, it means there is no path from here
        if (minPathWeight == Long.MAX_VALUE) {
            return Collections.emptyList();
        } else {
            this.currentBestRisk = Math.min(this.currentBestRisk, getPathRisk(bestPath));

            return bestPath;
        }
    }

    @Override
    public long partA() {
        List<Coordinate> bestPath = traverseCave(this.map.size()-1, this.map.get(0).size()-1, path);

        if (bestPath != null) {
            logger.debug("=== === === === === === === === === === === === === === ===");
            bestPath.forEach(e -> logger.debug("({}, {}) -> {}", e.getColumn(), e.getRow(), this.map.get(e.getRow()).get(e.getColumn())));
            logger.debug("=== === === === === === === === === === === === === === ===");

            return bestPath.stream().mapToLong(c -> this.map.get(c.getRow()).get(c.getColumn())).sum();
        } else {
            logger.error("FAILED TO FIND A PATH!");
        }

        return super.partA();
    }

    @Override
    public long partB() {
        return super.partB();
    }
}
