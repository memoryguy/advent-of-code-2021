package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Day19 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day19.class);

    public Day19(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    public long partA() {
        return super.partA();
    }

    @Override
    public long partB() {
        return super.partB();
    }
}
