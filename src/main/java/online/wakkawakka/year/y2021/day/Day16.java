package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day16.Packet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Day16 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day16.class);
    private static Map<String, String> binMap;

    private String inputBinary;

    public Day16(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        Day16.binMap = new TreeMap<>();

        Day16.binMap.put("0", "0000");
        Day16.binMap.put("1", "0001");
        Day16.binMap.put("2", "0010");
        Day16.binMap.put("3", "0011");
        Day16.binMap.put("4", "0100");
        Day16.binMap.put("5", "0101");
        Day16.binMap.put("6", "0110");
        Day16.binMap.put("7", "0111");
        Day16.binMap.put("8", "1000");
        Day16.binMap.put("9", "1001");
        Day16.binMap.put("A", "1010");
        Day16.binMap.put("B", "1011");
        Day16.binMap.put("C", "1100");
        Day16.binMap.put("D", "1101");
        Day16.binMap.put("E", "1110");
        Day16.binMap.put("F", "1111");
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        this.inputBinary = Arrays.stream(inputLine.trim().split(""))
                .map(binMap::get)
                .collect(Collectors.joining(""));
    }

    @Override
    public long partA() {
        logger.debug("Read input bits: {}", this.inputBinary);

        Packet sequence = Packet.newPacket(this.inputBinary);
        logger.debug("Packet version: {}", sequence.getHeader().getVersion());
        logger.debug("Packet type: {}", sequence.getHeader().getTypeId());

        return sequence.getVersionSum();
    }

    @Override
    public long partB() {
        logger.debug("Read input bits: {}", this.inputBinary);

        Packet sequence = Packet.newPacket(this.inputBinary);
        logger.debug("Packet version: {}", sequence.getHeader().getVersion());
        logger.debug("Packet type: {}", sequence.getHeader().getTypeId());
        logger.debug("Packet value: {}", sequence.getValue());

        return sequence.getValue();
    }
}
