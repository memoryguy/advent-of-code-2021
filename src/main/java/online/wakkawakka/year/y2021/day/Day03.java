package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day03.BitCounter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day03 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day03.class);

    private List<BitCounter> bits = new ArrayList<>();
    private List<BitCounter> o2Bits = new ArrayList<>();
    private List<BitCounter> co2Bits = new ArrayList<>();

    public Day03(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.bits = new ArrayList<>();
        this.o2Bits = new ArrayList<>();
        this.co2Bits = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        char[] bitChars = inputLine.toCharArray();

        // ensure that we have enough BitCounters for size of the
        // input. Ideally this will only run on the first iteration of
        // the loop, but would technically support dynamic sizes
        // through the input file
        while (bits.size() < bitChars.length) {
            bits.add(new BitCounter());

            o2Bits.add(new BitCounter());
            co2Bits.add(new BitCounter());
        }

        // update the running tally for each bit of the input line
        for (int i = 0; i < bitChars.length; i++) {
            bits.get(i).input(bitChars[i]);
        }
    }

    @Override
    public long partA() {
        try {
            // go through each BitCounter and get the most/least common value
            // for each bit. Then assemble those together into a string. This
            // represents the binary value for gamma/epsilon
            String gammaBits = bits.stream()
                    .map(BitCounter::getMostCommon)
                    .map(String::valueOf)
                    .collect(Collectors.joining());
            String epsilonBits = bits.stream()
                    .map(BitCounter::getLeastCommon)
                    .map(String::valueOf)
                    .collect(Collectors.joining());

            logger.debug("gammaBits: {}", gammaBits);
            logger.debug("epsilonBits: {}", epsilonBits);

            // convert the gamma and epsilon values to integers
            long gammaValue = Long.valueOf(gammaBits, 2);
            long epsilonValue = Long.valueOf(epsilonBits, 2);

            logger.debug("gammaValue: {}", gammaValue);
            logger.debug("epsilonValue: {}", epsilonValue);
            logger.debug("Product: {}", gammaValue * epsilonValue);

            return gammaValue * epsilonValue;
        } catch (NumberFormatException e) {
            logger.error("Unable to parse value");
        }

        return super.partA();
    }

    @Override
    public long partB() {
        try {
            int bitPosition = 0;
            int numBits;

            numBits = inputLines.get(0).length();
            List<String> o2Lines = inputLines;
            List<String> co2Lines = inputLines;

            while ((o2Lines.size() > 1 || co2Lines.size() > 1) && bitPosition < numBits) {
                final int tmpBitPos = bitPosition;

                logger.debug("Filtering on bit {}", tmpBitPos);
                logger.debug("  o2Lines.size: {}", o2Lines.size());
                logger.debug("  co2Lines.size: {}", co2Lines.size());

                // if there is more than one number remaining in the list,
                // find all the remaining lines that have the bit at the
                // current position equal to the most/least common bit value in
                // this position, over the entire (remaining) list
                //
                // it is important to remember that the bit tallies need to be
                // updated whenever the content of the list changes
                if (o2Lines.size() > 1) {
                    o2Lines = o2Lines.stream()
                            .filter(l -> l.charAt(tmpBitPos) == o2Bits.get(tmpBitPos).getMostCommon())
                            .collect(Collectors.toList());

                    recalculateBitCount(o2Lines, o2Bits);
                }

                if (co2Lines.size() > 1) {
                    co2Lines = co2Lines.stream()
                            .filter(l -> l.charAt(tmpBitPos) == co2Bits.get(tmpBitPos).getLeastCommon())
                            .collect(Collectors.toList());

                    recalculateBitCount(co2Lines, co2Bits);
                }

                logger.debug("O2 after filtering: {}", o2Lines);
                logger.debug("CO2 after filtering: {}", co2Lines);

                bitPosition++;
            }

            logger.debug("After filtering, o2Lines has {} ; co2Lines has {}", o2Lines.size(), co2Lines.size());

            long o2Value = Long.valueOf(o2Lines.get(0), 2);
            long co2Value = Long.valueOf(co2Lines.get(0), 2);

            logger.debug("o2 bits: {}", o2Lines.get(0));
            logger.debug("co2 bits: {}", co2Lines.get(0));
            logger.debug("o2Value: {}", o2Value);
            logger.debug("co2Value: {}", co2Value);
            logger.debug("Product: {}", o2Value*co2Value);

            return o2Value*co2Value;
        } catch (NumberFormatException e) {
            logger.error("Unable to parse value", e);
        }

        return super.partB();
    }

    private void recalculateBitCount(List<String> lines, List<BitCounter> counters) {
        counters.forEach(BitCounter::reset);

        lines.forEach(l -> {
            char[] bitChars = l.toCharArray();

            for (int i = 0; i < bitChars.length; i++) {
                counters.get(i).input(bitChars[i]);
            }
        });
    }
}
