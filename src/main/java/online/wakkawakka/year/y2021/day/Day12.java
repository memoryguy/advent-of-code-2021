package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day12.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Day12 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day12.class);
    private static final String CAVE_START = "start";
    private static final String CAVE_END = "end";

    private List<Connection> connections;

    public Day12(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.connections = new ArrayList<>();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        connections.add(new Connection(inputLine.trim()));
    }

    private boolean isBigCave(String cave) {
        return cave.toUpperCase().equals(cave);
    }

    private long traverseCaveA(List<String> pathSoFar, String cave) {
        List<String> nextPath = new ArrayList<>(pathSoFar);
        nextPath.add(cave);

        // if we're at the end, exit now; this is a single valid path
        if (cave.equals(CAVE_END)) {
            return 1;
        }

        // find all the connections from this cave, which are either BIG caves,
        // or are small and have not already been visited
        List<String> cavesToVisit = this.connections.stream()
                .filter(c -> c.connects(cave)).map(c -> c.getTo().equals(cave) ? c.getFrom() : c.getTo())
                .filter(c -> isBigCave(c) || !pathSoFar.contains(c))
                .collect(Collectors.toList());

        // recursively visit each of the connected caves and return the sum of
        // completed paths
        return cavesToVisit.stream()
                .mapToLong(c -> traverseCaveA(nextPath, c))
                .sum();
    }

    private boolean validDuplicateCaves(List<String> path) {
        Map<String, Integer> smallCaveCount = new TreeMap<>();

        // count the number of times we've visited each SMALL cave
        path.stream().filter(c -> !isBigCave(c))
                .forEach(c -> smallCaveCount.put(c, smallCaveCount.getOrDefault(c, 0)+1));

        // if START or END are visited more than once, then this is not a valid duplicate cave.
        // Otherwise, we only want to allow a single small cave with a visit count == 2
        if (smallCaveCount.getOrDefault(CAVE_START, 1) > 1 || smallCaveCount.getOrDefault(CAVE_END, 0) > 1) {
            return false;
        } else {
            return smallCaveCount.values().stream().filter(v -> v > 1).count() <= 1
                    && smallCaveCount.values().stream().noneMatch(v -> v > 2);
        }
    }

    private long traverseCaveB(List<String> pathSoFar, String cave) {
        List<String> nextPath = new ArrayList<>(pathSoFar);
        nextPath.add(cave);

        // if we're at the end, exit now; this is a single valid path
        // if we've visiting an invalid duplicate cave, exit now; this is NOT a
        // valid path
        if (cave.equals(CAVE_END)) {
            return 1;
        } else if (!validDuplicateCaves(nextPath)) {
            return 0;
        }

        // get all the connected caves
        List<String> cavesToVisit = this.connections.stream()
                .filter(c -> c.connects(cave)).map(c -> c.getTo().equals(cave) ? c.getFrom() : c.getTo())
                .collect(Collectors.toList());

        // recursively visit each of the connected caves and return the sum of
        // completed paths
        return cavesToVisit.stream()
                .mapToLong(c -> traverseCaveB(nextPath, c))
                .sum();
    }

    @Override
    public long partA() {
        return traverseCaveA(new ArrayList<>(), CAVE_START);
    }

    @Override
    public long partB() {
        return traverseCaveB(new ArrayList<>(), CAVE_START);
    }
}
