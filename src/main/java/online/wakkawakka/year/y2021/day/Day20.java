package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Day20 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day20.class);

    private List<Integer> filter;
    private List<List<Integer>> image;
    private int step;
    private boolean readFilter = false;

    public Day20(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.filter = new ArrayList<>();
        this.image = new ArrayList<>();
        this.readFilter = false;
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        if (!inputLine.isEmpty()) {
            // convert the line into a list of integers; # is 1, . is 0
            List<Integer> parsedLine = Arrays.stream(inputLine.trim().split(""))
                    .map(c -> Objects.equals(c, "#") ? 1 : 0)
                    .collect(Collectors.toList());

            if (readFilter) {
                image.add(parsedLine);
            } else {
                filter.addAll(parsedLine);
                readFilter = true;
            }
        }
    }

    private Integer getInputValue(List<List<Integer>> input, int x, int y) {
        if (y < 0 || y > input.size()-1 || x < 0 || x > input.get(y).size()-1) {
            // the value outside the grid may be 0 (for the sample data) or it
            // might flip back and forth (for the test data).
            //
            //   o The test data filter has # in the first position and . in
            //     the last, which leads to flipping
            //   o The sample data filter has . in the first position which
            //     stays as 0
            //
            // Thanks to the Subreddit for the hint
            return this.sampleData ? 0 : step % 2;
        }

        return input.get(y).get(x);
    }

    private List<List<Integer>> processImage(List<List<Integer>> input) {
        final List<List<Integer>> outputImage = new ArrayList<>();

        for (int y = -1; y <= input.size(); y++) {
            List<Integer> outputLine = new ArrayList<>();

            for (int x = -1; x <= input.size(); x++) {
                int indexBits = Integer.parseInt(String.format("%d%d%d%d%d%d%d%d%d",
                        getInputValue(input, x-1, y-1),
                        getInputValue(input, x, y-1),
                        getInputValue(input, x+1, y-1),

                        getInputValue(input, x-1, y),
                        getInputValue(input, x, y),
                        getInputValue(input, x+1, y),

                        getInputValue(input, x-1, y+1),
                        getInputValue(input, x, y+1),
                        getInputValue(input, x+1, y+1)), 2
                );

                outputLine.add(this.filter.get(indexBits));
            }

            outputImage.add(outputLine);
        }

        return outputImage;
    }

    private void displayImage(List<List<Integer>> displayImage) {
        if (logger.isDebugEnabled()) {
            logger.debug("Displaying image ({}x{}) =================", displayImage.get(0).size(), displayImage.size());
            displayImage.stream()
                    .map(c -> c.stream()
                            .map(cc -> cc == 1 ? "#" : ".")
                            .collect(Collectors.joining("")))
                    .collect(Collectors.toList()).forEach(logger::debug);
            logger.debug("==================================");
        }
    }

    @Override
    public long partA() {
        displayImage(this.image);

        List<List<Integer>> outputImage = this.image;
        for (step = 0; step < 2; step++) {
            outputImage = processImage(outputImage);

            displayImage(outputImage);
        }

        return outputImage.stream()
                .mapToLong(c -> c.stream()
                        .mapToLong(cc -> cc)
                        .sum())
                .sum();
    }

    @Override
    public long partB() {
        displayImage(this.image);

        List<List<Integer>> outputImage = this.image;
        for (step = 0; step < 50; step++) {
            outputImage = processImage(outputImage);

            displayImage(outputImage);
        }

        return outputImage.stream()
                .mapToLong(c -> c.stream()
                        .mapToLong(cc -> cc)
                        .sum())
                .sum();
    }
}
