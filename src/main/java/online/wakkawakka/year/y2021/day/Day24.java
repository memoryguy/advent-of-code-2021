package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day24.Monad;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Random;

public class Day24 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day24.class);

    private Monad monad;

    public Day24(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.monad = new Monad();
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        this.monad.add(inputLine);
    }

    @Override
    public long partA() {
        Random rnd = new Random();
        for (int randIter = 0; randIter < 10; randIter++) {
            String num = "";
            for (int digit = 0; digit < 14; digit++) {
                num += rnd.nextInt(9)+1;
            }

            this.monad.runProgram(num);

            logger.info("After execution: {} == {}", num, this.monad.getRegisters().get("z"));
        }

        this.monad.runProgram("99429795993929");

        return this.monad.getRegisters().get("z");
    }

    @Override
    public long partB () {
        return super.partB();
    }
}
