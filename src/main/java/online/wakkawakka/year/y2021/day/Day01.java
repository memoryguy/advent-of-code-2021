package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Day01 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day01.class);

    public Day01(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    public long partA() {
        long increasingCount = 0;
        long lastValue = -1;
        long currentValue = -1;

        try {
            for (String readValue : inputLines) {
                if (lastValue == -1) {
                    lastValue = Long.parseLong(readValue);
                    continue;
                }

                currentValue = Long.parseLong(readValue);

                if (currentValue > lastValue) {
                    increasingCount++;
                }

                lastValue = currentValue;
            }

            return increasingCount;
        } catch (NumberFormatException e) {
            logger.error("Not a recognized number format");
        }

        return super.partA();
    }

    @Override
    public long partB() {
        List<Long> window = new ArrayList<>();

        try {
            long increasingCount = 0;
            long lastValue = 0;
            long currentValue = 0;

            for (String readValue : inputLines) {
                // populate the sliding window with the first 3 values from the input
                if (window.size() < 3) {
                    window.add(Long.parseLong(readValue));

                    if (window.size() == 3) {
                        lastValue = window.stream().reduce(0L, Long::sum);
                    }

                    continue;
                }

                // moving the sliding window can be accomplished by removing
                // the first element and adding the new value from the input to
                // the end
                window.remove(0);
                window.add(Long.parseLong(readValue));

                currentValue = window.stream().reduce(0L, Long::sum);

                if (currentValue > lastValue) {
                    increasingCount++;
                }

                lastValue = currentValue;
            }

            return increasingCount;
        } catch (NumberFormatException e) {
            logger.error("Not a recognized number format");
        }

        return super.partB();
    }
}
