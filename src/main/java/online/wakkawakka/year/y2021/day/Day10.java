package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day10.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Day10 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day10.class);
    private static final String OPENINGS = "([{<";
    private static final String CLOSINGS = ")]}>";
    private static final Map<String, Integer> corruptedScoreMap = new TreeMap<>();
    private static final Map<String, Integer> incompleteScoreMap = new TreeMap<>();

    private final Deque<Operator> stack = new ArrayDeque<>();

    public Day10(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);

        corruptedScoreMap.put(")", 3);
        corruptedScoreMap.put("]", 57);
        corruptedScoreMap.put("}", 1197);
        corruptedScoreMap.put(">", 25137);

        incompleteScoreMap.put(")", 1);
        incompleteScoreMap.put("]", 2);
        incompleteScoreMap.put("}", 3);
        incompleteScoreMap.put(">", 4);
    }

    @Override
    public long partA() {
        long totalScore = 0;

        for (String input : inputLines) {
            totalScore += calculateCorruptionScore(input);
        }

        return totalScore;
    }

    @Override
    public long partB() {
        List<Long> totalScores = new ArrayList<>();

        for (String input : inputLines) {
            long totalScore = 0;

            if (calculateCorruptionScore(input) == 0) {
                while (!stack.isEmpty()) {
                    Operator closer = stack.pop();

                    totalScore = totalScore * 5 + incompleteScoreMap.get(closer.getClosing());
                }

                totalScores.add(totalScore);
            }
        }

        Collections.sort(totalScores);
        logger.debug("Total scores: {}", totalScores);

        return totalScores.get(totalScores.size()/2);
    }

    private long calculateCorruptionScore(String input) {
        long corruptedScore = 0;

        stack.clear();

        for (String character : input.split("")) {
            if (OPENINGS.contains(character)) {
                stack.push(new Operator(character));
            } else if (CLOSINGS.contains(character)) {
                // make sure the top of the stack has the matching
                // closer; that is, that we're closing the same chunk
                // type
                Operator lastChunk = stack.pop();

                if (!lastChunk.getClosing().equals(character)) {
                    corruptedScore += corruptedScoreMap.get(character);
                }
            }
        }

        return corruptedScore;
    }
}
