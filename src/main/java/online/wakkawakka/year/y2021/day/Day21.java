package online.wakkawakka.year.y2021.day;

import online.wakkawakka.year.DayBase;
import online.wakkawakka.year.y2021.helper.day21.UniverseResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCacheManager;

import java.io.IOException;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day21 extends DayBase {
    private static final Logger logger = LoggerFactory.getLogger(Day21.class);
    private static final Pattern startingPattern = Pattern.compile("^Player (?<player>\\d+) starting position: (?<position>\\d+)$");
    private static final String CACHE_NAME = "diracDice";
    private static final int WINNING_SCORE = 21;

    enum CacheMode {
        NONE, HASH, CAFFEINE, REDIS
    }
    private Day21Other.CacheMode cacheMode;

    private int[] players;
    private int[] scores;
    private int die = 0;
    private long rollCount = 0;

    private CacheManager cacheManager;
    private final Map<String, UniverseResult> UNIVERSE_RESULTS = new HashMap<>();

    public Day21(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        super(year, day, part, sampleData, variant);
    }

    @Override
    protected void initializeMembers() {
        this.players = new int[2];
        this.scores = new int[2];
    }

    @Override
    protected void processInputLine(String inputLine) throws NumberFormatException {
        Matcher matcher = startingPattern.matcher(inputLine.trim());

        if (matcher.find()) {
            int player = Integer.parseInt(matcher.group("player"));
            int position = Integer.parseInt(matcher.group("position"));

            logger.debug("Setting player {} to position {}", player, position);

            players[player-1] = position-1;
        }
    }

    private int rollDie(int sided) {
        this.die = (this.die % sided) + 1;
        this.rollCount++;

        return this.die;
    }

    @Override
    public long partA() {
        final int TARGET_SCORE = 1000;
        final int DIE_SIZE = 100;

        while (this.scores[0] < TARGET_SCORE && this.scores[1] < TARGET_SCORE) {
            for (int id = 0; id < 2; id++) {
                int roll = rollDie(DIE_SIZE) + rollDie(DIE_SIZE) + rollDie(DIE_SIZE);

                logger.debug("Player {} rolled a total of {} and moves from {} to {}, total score {}",
                        id+1,
                        roll,
                        this.players[id]+1,
                        ((this.players[id] + roll) % 10) + 1,
                        scores[id] + ((this.players[id] + roll) % 10) + 1);

                this.players[id] = (this.players[id] + roll) % 10;
                this.scores[id] += this.players[id]+1;

                if (this.scores[id] >= TARGET_SCORE) break;
            }
        }

        return this.rollCount * (scores[0] < TARGET_SCORE ? scores[0] : scores[1]);
    }

    @Override
    public long partB() {
        int p1Position = this.players[0];
        int p2Position = this.players[1];

        Instant hashStart = Instant.now();
        this.cacheMode = Day21Other.CacheMode.HASH;
        this.cacheManager = new CaffeineCacheManager(CACHE_NAME);
        UniverseResult result = playGame(0, 0, p1Position, p2Position, 0, 0);
        Instant hashEnd = Instant.now();

        logger.info("Hash P1: {}", result.getP1Wins());
        logger.info("Hash P2: {}", result.getP2Wins());

        Instant caffeineStart = Instant.now();
        this.cacheMode = Day21Other.CacheMode.CAFFEINE;
        this.cacheManager = new CaffeineCacheManager(CACHE_NAME);
        result = playGame(0, 0, p1Position, p2Position, 0, 0);
        Instant caffeineEnd = Instant.now();

        logger.info("Caffeine P1: {}", result.getP1Wins());
        logger.info("Caffeine P2: {}", result.getP2Wins());

//        Instant nopStart = Instant.now();
//        this.cacheMode = CacheMode.NONE;
//        this.cacheManager = new NoOpCacheManager();
//        result = playGame(0, 0, p1Position, p2Position, 0, 0, "none");
//        Instant nopEnd = Instant.now();
//
//        logger.info("Uncached P1: {}", result.getP1Wins());
//        logger.info("Uncached P2: {}", result.getP2Wins());

        logger.info("Hash duration: {}", Duration.between(hashStart, hashEnd).toMillis());
        logger.info("Caffeine duration: {}", Duration.between(caffeineStart, caffeineEnd).toMillis());
//        logger.info("Uncached duration: {}", Duration.between(nopStart, nopEnd).toMillis());

        return Math.max(result.getP1Wins().longValue(), result.getP2Wins().longValue());
    }

    private int endPosition(int position, int diceRoll) {
        return ((position-1 + diceRoll) % 10) + 1;
    }

    private UniverseResult getCachedResult(String cacheKey) {
        if (this.cacheMode == Day21Other.CacheMode.HASH) {
            return UNIVERSE_RESULTS.getOrDefault(cacheKey, null);
        } else {
            Cache cache = this.cacheManager.getCache(CACHE_NAME);
            if (cache != null) {
                Cache.ValueWrapper previousResult = cache.get(cacheKey);

                if (previousResult != null) {
                    return (UniverseResult) previousResult.get();
                }
            }
        }

        return null;
    }

    private void saveCachedResult(String cacheKey, UniverseResult result) {
        if (this.cacheMode == Day21Other.CacheMode.HASH) {
            UNIVERSE_RESULTS.put(cacheKey, result);
        } else {
            Cache cache = this.cacheManager.getCache(CACHE_NAME);
            if (cache != null) {
                cache.put(cacheKey, result);
            }
        }
    }

    public UniverseResult playGame(int p1Score, int p2Score, int p1Position, int p2Position, int turn, int throwSum) {
        boolean isP1Turn = turn < 3;
        boolean lastP1Throw = turn == 2;
        boolean lastP2Throw = turn == 5;

        String cacheKey = String.format("%d-%d-%d-%d-%d-%d", p1Position, p1Score, p2Position, p2Score, turn, throwSum);

        UniverseResult res;

        if (p1Score >= WINNING_SCORE) {
            res = new UniverseResult(cacheKey, BigInteger.ONE, BigInteger.ZERO);
        } else if (p2Score >= WINNING_SCORE) {
            res = new UniverseResult(cacheKey, BigInteger.ZERO, BigInteger.ONE);
        } else {
            UniverseResult cachedResult = getCachedResult(cacheKey);
            if (cachedResult != null) {
                return cachedResult;
            }

            int nextTurn = (turn+1) % 6;
            BigInteger p1Wins = BigInteger.ZERO;
            BigInteger p2Wins = BigInteger.ZERO;

            for (int roll = 1; roll <= 3; roll++) {
                UniverseResult result = playGame(
                        isP1Turn ? p1Score + (lastP1Throw ? endPosition(p1Position, roll) : 0) : p1Score,
                        !isP1Turn ? p2Score + (lastP2Throw ? endPosition(p2Position, roll) : 0) : p2Score,
                        isP1Turn ? endPosition(p1Position, roll) : p1Position,
                        !isP1Turn ? endPosition(p2Position, roll) : p2Position,
                        nextTurn,
                        turn % 3 == 0 ? 0 : throwSum + roll
                );

                p1Wins = p1Wins.add(result.getP1Wins());
                p2Wins = p2Wins.add(result.getP2Wins());
            }

            res = new UniverseResult(cacheKey, p1Wins, p2Wins);
        }

        saveCachedResult(cacheKey, res);
        return res;
    }
}
