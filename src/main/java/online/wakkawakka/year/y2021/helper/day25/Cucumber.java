package online.wakkawakka.year.y2021.helper.day25;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@ToString
public class Cucumber {
    private static final Logger logger = LoggerFactory.getLogger(Cucumber.class);

    private int posX;
    private int posY;

    public void incPosX(int maxSize) {
        this.posX = (this.posX+1) % maxSize;
    }

    public void incPosY(int maxSize) {
        this.posY = (this.posY+1) % maxSize;
    }
}
