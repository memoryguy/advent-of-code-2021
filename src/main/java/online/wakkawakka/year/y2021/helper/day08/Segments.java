package online.wakkawakka.year.y2021.helper.day08;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class Segments extends ArrayList<Character> {
    private static final String SEGMENT_NAMES = "abcdefg";

    private int value;
    private boolean[] segmentArray = new boolean[7];

    public Segments() {
        this.value = -1;
    }

    public Segments(String segments) {
        addAll(Arrays.stream(segments.split("")).map(s -> s.toCharArray()[0]).collect(Collectors.toList()));

        forEach(e -> this.segmentArray[SEGMENT_NAMES.indexOf(e)] = true);

        switch (size()) {
            case 2: this.value = 1; break;
            case 3: this.value = 7; break;
            case 4: this.value = 4; break;
            case 7: this.value = 8; break;
            default: this.value = -1; break;
        }
    }

    public int similarity(Segments compareTo) {
        int differences = 0;

        for (int segment = 0; segment < 7; segment++) {
            differences += segmentArray[segment] ^ compareTo.segmentArray[segment] ? 1 : 0;
        }

        return differences;
    }

    // find the value in <compareTo> that has the most segments in common with
    // this one
    public Segments mostSimilarTo(List<Segments> compareTo) {
        int smallest = 7;
        Segments smallestNum = new Segments();

        for (Segments v : compareTo) {
            int differences = similarity(v);

            if (differences < smallest) {
                smallest = differences;
                smallestNum = v;
            }
        }

        return smallestNum;
    }

    private String segmentsToString(boolean[] bits) {
        String toString = "";

        for (int i = 0; i < bits.length; i++) {
            toString += bits[i] ? SEGMENT_NAMES.charAt(i) : "";
        }

        return toString;
    }

    public Segments add(Segments other) {
        Segments result = new Segments();

        for (int segment = 0; segment < 7; segment++) {
            result.segmentArray[segment] = segmentArray[segment] || other.segmentArray[segment];
        }

        return new Segments(segmentsToString(result.segmentArray));
    }

    public Segments subtract(Segments other) {
        Segments result = new Segments();

        for (int segment = 0; segment < 7; segment++) {
            if (segmentArray[segment] && other.segmentArray[segment]) {
                result.segmentArray[segment] = false;
            } else {
                result.segmentArray[segment] = segmentArray[segment];
            }
        }

        return new Segments(segmentsToString(result.segmentArray));
    }
}
