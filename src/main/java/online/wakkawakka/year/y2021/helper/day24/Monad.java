package online.wakkawakka.year.y2021.helper.day24;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Getter
public class Monad extends ArrayList<Operation> {
    private static final Logger logger = LoggerFactory.getLogger(Monad.class);
    private static final String OP_INPUT = "inp";
    private static final String OP_ADD = "add";
    private static final String OP_MULTIPLY = "mul";
    private static final String OP_DIVIDE = "div";
    private static final String OP_MODULUS = "mod";
    private static final String OP_EQUALS = "eql";

    private final Map<String, Integer> registers = new TreeMap<>();

    private List<Integer> userInput;
    private int userInputIndex;

    public Monad() {
        super();

        reset();
    }

    public void setUserInput(String userInput) {
        this.userInput = Arrays.stream(userInput.split("")).map(Integer::parseInt).collect(Collectors.toList());
        this.userInputIndex = 0;
    }

    public void reset() {
        this.registers.put("w", 0);
        this.registers.put("x", 0);
        this.registers.put("y", 0);
        this.registers.put("z", 0);

        this.userInputIndex = 0;
    }

    public void add(String input) {
        this.add(new Operation(input));
    }

    public void runProgram(String userInput) {
        setUserInput(userInput);
        reset();

        for (Operation op : this) {
            perform(op);
        }
    }

    private int getBValue(Object regB) {
        if (regB == null) {
            return 0;
        }

        boolean isImmediate = regB instanceof Integer;

        return isImmediate ? ((Integer) regB) : this.registers.get((String)regB);
    }

    private void perform(Operation op) {
        String regA = op.getA();
        Integer regB = getBValue(op.getB());
        int result;

        switch (op.getOp()) {
            case OP_INPUT:
                this.registers.put(regA, this.userInput.get(this.userInputIndex));
                logger.debug("Executing: INPUT {} -> {}   STATE: {}", this.userInput.get(this.userInputIndex), regA, getState());
                this.userInputIndex++;
                break;
            case OP_ADD:
                result = this.registers.get(regA) + regB;

                registers.put(regA, result);
                logger.debug("Executing: ADD {} + {} -> {}   STATE: {}", regA, op.getB(), result, getState());
                break;
            case OP_MULTIPLY:
                result = this.registers.get(regA) * regB;

                registers.put(regA, result);
                logger.debug("Executing: MULTIPLY {} * {} -> {}   STATE: {}", regA, op.getB(), result, getState());
                break;
            case OP_DIVIDE:
                result = Math.floorDiv(this.registers.get(regA), regB);

                registers.put(regA, result);
                logger.debug("Executing: DIVIDE {} / {} -> {}   STATE: {}", regA, op.getB(), result, getState());
                break;
            case OP_MODULUS:
                result = this.registers.get(regA) % regB;

                registers.put(regA, result);
                logger.debug("Executing: MOD {} % {} -> {}   STATE: {}", regA, op.getB(), result, getState());
                break;
            case OP_EQUALS:
                result = this.registers.get(regA).equals(regB) ? 1 : 0;

                registers.put(regA, result);
                logger.debug("Executing: EQUALS {} == {} -> {}   STATE: {}", regA, op.getB(), result, getState());
                break;
            default:
                logger.error("UNKNOWN INSTRUCTION: {}", op.getOp());
        }
    }

    public String getState() {
        return String.format("{ w: %d, x: %d, y: %d, z: %d }",
                this.registers.get("w"),
                this.registers.get("x"),
                this.registers.get("y"),
                this.registers.get("z")
        );
    }
}
