package online.wakkawakka.year.y2021.helper.day10;

import lombok.Getter;

@Getter
public class Operator {
    private final String opening;
    private final String closing;

    public Operator(String opening) {
        this.opening = opening;

        switch (opening) {
            case "{": this.closing = "}"; break;
            case "(": this.closing = ")"; break;
            case "[": this.closing = "]"; break;
            case "<": this.closing = ">"; break;
            default:
                throw new IllegalArgumentException("Chunk opener not recognized: " + opening);
        }
    }
}
