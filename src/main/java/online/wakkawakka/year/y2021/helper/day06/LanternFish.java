package online.wakkawakka.year.y2021.helper.day06;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanternFish {
    private byte counter;

    public LanternFish() {
        this.counter = 8;
    }

    public LanternFish(byte counter) {
        this.counter = counter;
    }

    @Override
    public String toString() {
        return String.valueOf(counter);
    }

    public void decrement() {
        counter--;
    }

    public void reset() {
        counter = 6;
    }
}
