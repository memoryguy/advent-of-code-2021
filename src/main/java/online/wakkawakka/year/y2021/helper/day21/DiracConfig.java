package online.wakkawakka.year.y2021.helper.day21;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class DiracConfig {
    private static final Logger logger = LoggerFactory.getLogger(DiracConfig.class);

    private int player1Position;
    private int player2Position;
    private int player1Score;
    private int player2Score;
    private boolean player1Turn;

    public boolean isPlayer1Winner() {
        return this.player1Score >= 21;
    }

    public boolean isPlayer2Winner() {
        return this.player2Score >= 21;
    }

    public DiracConfig play(int roll) {
        if (isPlayer1Winner() || isPlayer2Winner()) {
            String message = String.format("Game has already completed (Player %d won)", isPlayer1Winner() ? 1 : 2);
            throw new IllegalStateException(message);
        }

        if (this.player1Turn) {
            int newPlayer1Position = (((this.player1Position + roll)) % 10);
            int newPlayer1Score = this.player1Score + newPlayer1Position;

            return new DiracConfig(newPlayer1Position, this.player2Position, newPlayer1Score, this.player2Score, false);
        } else {
            int newPlayer2Position = (((this.player2Position + roll)) % 10);
            int newPlayer2Score = this.player2Score + newPlayer2Position;

            return new DiracConfig(this.player1Position, newPlayer2Position, this.player1Score, newPlayer2Score, true);
        }
    }
}
