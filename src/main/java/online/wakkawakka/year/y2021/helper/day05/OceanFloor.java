package online.wakkawakka.year.y2021.helper.day05;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OceanFloor {
    private static final Logger logger = LoggerFactory.getLogger(OceanFloor.class);

    private final int[][] floor;
    private final int maxx;
    private final int maxy;

    public OceanFloor(int maxx, int maxy) {
        this.floor = new int[maxy][maxx];
        this.maxx = maxx;
        this.maxy = maxy;
    }
    
    public void markLine(Line line) {
        Coordinate start = line.getStart();
        Coordinate end = line.getEnd();
        Coordinate current = new Coordinate(start.getX(), start.getY());

        // make sure we're within the range of the map (sanity check)
        if (start.getX() < 0 || start.getX() >= maxx) {
            throw new IllegalArgumentException("Start x position is out of range: " + start.getX());
        } else if (start.getY() < 0 || start.getY() >= maxy) {
            throw new IllegalArgumentException("Start y position is out of range: " + start.getY());
        } else if (end.getX() < 0 || end.getX() >= maxx) {
            throw new IllegalArgumentException("End x position is out of range: " + end.getX());
        } else if (end.getY() < 0 || end.getY() >= maxy) {
            throw new IllegalArgumentException("End y position is out of range: " + end.getY());
        }

        // mark the current position and then move the X and Y coordinates one
        // step closer to the end position. We know that the line will either
        // be horizontal, vertical, or a 45-degree angle, so we don't have to
        // worry about the slope of the line
        while (!current.equals(end)) {
            floor[current.getY()][current.getX()]++;

            if (current.getX() < end.getX()) {
                current.incX();
            } else if (current.getX() > end.getX()) {
                current.decX();
            }

            if (current.getY() < end.getY()) {
                current.incY();
            } else if (current.getY() > end.getY()) {
                current.decY();
            }
        }

        // the final position IS included in the line
        floor[current.getY()][current.getX()]++;
    }

    public long overlapPointCount() {
        long count = 0;

        for (int y = 0; y < this.maxy; y++) {
            for (int x = 0; x < this.maxx; x++) {
                if (floor[y][x] > 1) {
                    count++;
                }
            }
        }

        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int y = 0; y < this.maxy; y++) {
            for (int x = 0; x < this.maxx; x++) {
                sb.append(this.floor[y][x]).append(" ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }
}
