package online.wakkawakka.year.y2021.helper.day11;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Octopus {
    private int powerLevel;
    private boolean flashed;
    private int flashCount;

    public Octopus(int initalPowerLevel) {
        this.powerLevel = initalPowerLevel;
        this.flashed = false;
        this.flashCount = 0;
    }

    public void increasePowerLevel() {
        this.powerLevel++;
    }

    public boolean canFlash() {
        if (this.powerLevel > 9 && !this.flashed) {
            this.flashed = true;
            this.flashCount++;

            return true;
        } else {
            return false;
        }
    }

    public void resetFlash() {
        if (this.powerLevel > 9) {
            this.powerLevel = 0;
        }

        this.flashed = false;
    }
}
