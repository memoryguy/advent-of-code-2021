package online.wakkawakka.year.y2021.helper.day12;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Connection {
    private String from;
    private String to;

    public Connection(String connectionString) {
        String[] parts = connectionString.split("\\s*-\\s*");

        this.from = parts[0];
        this.to = parts[1];
    }

    public boolean connects(String cave) {
        return this.from.equals(cave) || this.to.equals(cave);
    }
}
