package online.wakkawakka.year.y2021.helper.day24;

import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
public class Operation {
    private static final Logger logger = LoggerFactory.getLogger(Operation.class);

    private String op;
    private String a;
    private Object b;

    public Operation(String operation) {
        String[] parts = operation.split("\\s+");

        this.op = parts[0];
        this.a = parts[1];
        if (parts.length > 2) {
            this.b = (parts[2].matches("[wxyz]") ? parts[2] : Integer.valueOf(parts[2]));
        }
    }
}
