package online.wakkawakka.year.y2021.helper.day16;

public class SumPacket extends OperatorPacket {
    public SumPacket(PacketHeader header, String inputBits) {
        super(header, inputBits);
    }

    @Override
    public long getValue() {
        return subPackets.stream().mapToLong(Packet::getValue).sum();
    }
}
