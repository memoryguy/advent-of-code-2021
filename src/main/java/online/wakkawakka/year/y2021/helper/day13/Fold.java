package online.wakkawakka.year.y2021.helper.day13;

import lombok.Getter;
import lombok.Setter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@Setter
public class Fold {
    public enum Direction {
        FOLD_LEFT, FOLD_UP
    }

    private static final Pattern pattern = Pattern.compile("^fold along (?<direction>[xy])=(?<line>\\d+)$");

    private final Direction direction;
    private final int foldLine;

    public Fold(String input) throws NumberFormatException, IllegalArgumentException {
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            if (matcher.group("direction").equals("y")) {
                direction = Direction.FOLD_UP;
            } else {
                direction = Direction.FOLD_LEFT;
            }

            foldLine = Integer.parseInt(matcher.group("line"));
        } else {
            throw new IllegalArgumentException("Unrecognized fold input: " + input);
        }
    }
}
