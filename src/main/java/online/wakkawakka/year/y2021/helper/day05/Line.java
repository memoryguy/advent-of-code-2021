package online.wakkawakka.year.y2021.helper.day05;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Line {
    private Coordinate start;
    private Coordinate end;
}
