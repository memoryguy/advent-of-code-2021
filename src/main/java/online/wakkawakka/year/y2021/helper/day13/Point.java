package online.wakkawakka.year.y2021.helper.day13;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class Point {
    private int x;
    private int y;

    public Point(String input) throws NumberFormatException {
        String[] parts = input.split("\\s*,\\s*");

        x = Integer.parseInt(parts[0]);
        y = Integer.parseInt(parts[1]);
    }

    public Point(Point old) {
        this.x = old.x;
        this.y = old.y;
    }
}
