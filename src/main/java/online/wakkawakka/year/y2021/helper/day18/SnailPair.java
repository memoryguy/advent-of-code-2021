package online.wakkawakka.year.y2021.helper.day18;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
public class SnailPair {
    public static final int NO_VALUE = -1;

    private static final Logger logger = LoggerFactory.getLogger(SnailPair.class);
    private static boolean modified = false;

    private SnailPair leftChild;
    private SnailPair rightChild;
    private int depth;
    private int inputLength;

    private int valueLeft;
    private int valueRight;

    public SnailPair(String inputString, int depth) throws NumberFormatException {
        this.inputLength = 1;

        if (inputString.charAt(this.inputLength) == '[') {
            this.leftChild = new SnailPair(inputString.substring(this.inputLength), depth+1);
            this.inputLength += this.leftChild.getInputLength();
        } else {
            this.leftChild = null;
            this.valueLeft = Integer.parseInt(inputString.substring(this.inputLength, this.inputLength+1));
            this.inputLength++;
        }

        // skip over the comma
        this.inputLength++;
        if (inputString.charAt(this.inputLength) == '[') {
            this.rightChild = new SnailPair(inputString.substring(this.inputLength), depth+1);
            this.inputLength += this.rightChild.getInputLength();
        } else {
            this.rightChild = null;
            this.valueRight = Integer.parseInt(inputString.substring(this.inputLength, this.inputLength+1));
            this.inputLength++;
        }

        // skip over the closing bracket
        this.inputLength++;

        this.depth = depth;
    }

    public SnailPair(SnailPair left, SnailPair right, int depth) {
        this.leftChild = left;
        this.rightChild = right;
        this.depth = depth;

        this.leftChild.increaseDepth();
        this.rightChild.increaseDepth();
    }

    public void increaseDepth() {
        this.depth++;

        if (this.leftChild != null) {
            this.leftChild.increaseDepth();
        }
        if (this.rightChild != null) {
            this.rightChild.increaseDepth();
        }
    }

    public void decreaseDepth() {
        this.depth--;

        if (this.leftChild != null) {
            this.leftChild.decreaseDepth();
        }
        if (this.rightChild != null) {
            this.rightChild.decreaseDepth();
        }
    }

    private SnailPair getRightMostChildLeaf() {
        if (!hasRightChild()) {
            return this;
        } else {
            return this.rightChild.getRightMostChildLeaf();
        }
    }

    private SnailPair getLeftMostChildLeaf() {
        if (!hasLeftChild()) {
            return this;
        } else {
            return this.leftChild.getLeftMostChildLeaf();
        }
    }

    private SnailPair explode() {
        SnailPair toReturn = null;

        // check if THIS is a node that we need to reduce
        if (!hasChildren() && this.depth > 4) {
            logger.debug("Found item to explode: {}", this);
            return this;
        }

        // this node isn't one we need to explode, so look for one in the
        // children. Search left-first
        if (hasLeftChild()) {
            toReturn = this.leftChild.explode();
        }

        // if there is nothing on the left that needs exploding, try on the
        // right
        if (toReturn == null && hasRightChild()) {
            toReturn = this.rightChild.explode();
        }

        // if we found a sub-node to be exploded, find the right-most value on the left and the left-most value on the right
        if (toReturn != null) {
            if (toReturn == this.leftChild) {
                logger.debug("Applying 0 to LEFT child here {}", this);
                SnailPair.modified = true;
                this.leftChild = null;
                this.valueLeft = 0;

                return toReturn;
            }

            if (toReturn == this.rightChild) {
                logger.debug("Applying 0 to RIGHT child here {}", this);
                SnailPair.modified = true;
                this.rightChild = null;
                this.valueRight = 0;

                return toReturn;
            }

            SnailPair rightMost = hasLeftChild() ? this.leftChild.getRightMostChildLeaf() : null;
            SnailPair leftMost = hasRightChild() ? this.rightChild.getLeftMostChildLeaf() : null;

            if (rightMost != null) {
                logger.debug("right-most value on the left: {}", rightMost);
                logger.debug("  new value: {} + {} == {}", rightMost.valueRight, toReturn.valueLeft, rightMost.valueRight + toReturn.valueLeft);
                rightMost.valueRight += toReturn.valueLeft;
                toReturn.valueLeft = 0;
            }

            if (leftMost != null) {
                logger.debug("left-most value on the right: {}", leftMost);
                logger.debug("  new value: {} + {} == {}", leftMost.valueLeft, toReturn.valueRight, leftMost.valueLeft + toReturn.valueRight);
                leftMost.valueLeft += toReturn.valueRight;
                toReturn.valueRight = 0;
            }

            if (rightMost != null && leftMost != null) {
                toReturn = null;
            }
        }

        return toReturn;
    }

    private SnailPair split() {
        return null;
    }

    public void reduce() {
        do {
            SnailPair.modified = false;

            explode();
            if (!modified) {
                split();
            }

        } while (modified);
    }

    public boolean hasChildren() {
        return this.leftChild != null || this.rightChild != null;
    }

    public boolean hasLeftChild() {
        return this.leftChild != null;
    }

    public boolean hasRightChild() {
        return this.rightChild != null;
    }

    public long getMagnitude() {
        return 3*(this.leftChild == null ? this.valueLeft : this.leftChild.getMagnitude())
                + 2*(this.rightChild == null ? this.valueRight : this.rightChild.getMagnitude());
    }

    @Override
    public String toString() {
        return "["
                + (this.leftChild == null ? String.valueOf(this.valueLeft) : this.leftChild.toString())
                + ","
                + (this.rightChild == null ? String.valueOf(this.valueRight) : this.rightChild.toString())
                + "]";
    }
}
