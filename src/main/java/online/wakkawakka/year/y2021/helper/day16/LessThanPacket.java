package online.wakkawakka.year.y2021.helper.day16;

public class LessThanPacket extends OperatorPacket {
    public LessThanPacket(PacketHeader header, String inputBits) {
        super(header, inputBits);
    }

    @Override
    public long getValue() {
        return subPackets.get(0).getValue() < subPackets.get(1).getValue() ? 1 : 0;
    }
}
