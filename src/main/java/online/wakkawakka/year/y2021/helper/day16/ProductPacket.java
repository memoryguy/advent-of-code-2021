package online.wakkawakka.year.y2021.helper.day16;

public class ProductPacket extends OperatorPacket {
    public ProductPacket(PacketHeader header, String inputBits) {
        super(header, inputBits);
    }

    @Override
    public long getValue() {
        return subPackets.stream().mapToLong(Packet::getValue).reduce(1, (a, b) -> a*b);
    }
}
