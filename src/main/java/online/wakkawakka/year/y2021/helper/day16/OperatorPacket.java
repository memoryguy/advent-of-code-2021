package online.wakkawakka.year.y2021.helper.day16;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class OperatorPacket extends Packet {
    private static final Logger logger = LoggerFactory.getLogger(OperatorPacket.class);

    protected List<Packet> subPackets = new ArrayList<>();

    public OperatorPacket(PacketHeader header, String inputBits) {
        super(header, inputBits);

        boolean lengthInBits = inputBits.charAt(bitsConsumed++) == '0';

        if (lengthInBits) {
            int subPacketLength = Integer.parseInt(inputBits.substring(bitsConsumed, bitsConsumed+15), 2);
            bitsConsumed += 15;

            int consumed = 0;
            while (consumed < subPacketLength) {
                Packet newPacket = Packet.newPacket(inputBits.substring(bitsConsumed));
                bitsConsumed += newPacket.bitsConsumed;
                consumed += newPacket.bitsConsumed;

                subPackets.add(newPacket);
            }
        } else {
            int subPacketCount = Integer.parseInt(inputBits.substring(bitsConsumed, bitsConsumed+11), 2);
            bitsConsumed += 11;

            for (int packetIndex = 0; packetIndex < subPacketCount; packetIndex++) {
                Packet newPacket = Packet.newPacket(inputBits.substring(bitsConsumed));
                bitsConsumed += newPacket.bitsConsumed;

                subPackets.add(newPacket);
            }
        }
    }

    @Override
    public long getVersionSum() {
        return getHeader().getVersion() + subPackets.stream().mapToLong(Packet::getVersionSum).sum();
    }
}
