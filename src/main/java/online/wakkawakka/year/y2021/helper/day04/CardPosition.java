package online.wakkawakka.year.y2021.helper.day04;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
public class CardPosition {
    private static final Logger logger = LoggerFactory.getLogger(CardPosition.class);

    private int value;
    private boolean selected;

    CardPosition(String value) {
        logger.debug("CardPosition -> {}", value);

        this.value = Integer.parseInt(value);
        this.selected = false;
    }

    @Override
    public String toString() {
        return String.format("%s%2d%s",
                selected ? "[" : " ",
                value,
                selected ? "]" : " ");
    }
}
