package online.wakkawakka.year.y2021.helper.day16;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
public class Packet {
    private static final Logger logger = LoggerFactory.getLogger(Packet.class);

    private PacketHeader header;
    protected int bitsConsumed;

    public Packet(PacketHeader header, String inputBits) {
        this.header = header;
        this.bitsConsumed = 6; // size of header bits
    }

    public static Packet newPacket(String inputBits) {
        if (inputBits.length() < 6) {
            throw new IllegalArgumentException("Incomplete packet submitted: " + inputBits);
        }

        PacketHeader header = new PacketHeader(inputBits.substring(0, 7));
        Packet newPacket;

        switch (header.getTypeId()) {
            case 0:
                newPacket = new SumPacket(header, inputBits);
                break;
            case 1:
                newPacket = new ProductPacket(header, inputBits);
                break;
            case 2:
                newPacket = new MinimumPacket(header, inputBits);
                break;
            case 3:
                newPacket = new MaximumPacket(header, inputBits);
                break;
            case 4:
                newPacket = new LiteralIntegerPacket(header, inputBits);
                break;
            case 5:
                newPacket = new GreaterThanPacket(header, inputBits);
                break;
            case 6:
                newPacket = new LessThanPacket(header, inputBits);
                break;
            case 7:
                newPacket = new EqualToPacket(header, inputBits);
                break;
            default:
                newPacket = new OperatorPacket(header, inputBits);
        }

        return newPacket;
    }

    public long getValue() {
        throw new IllegalStateException("!! getValue not implemented!");
    }

    public long getVersionSum() {
        return header.getVersion();
    }
}
