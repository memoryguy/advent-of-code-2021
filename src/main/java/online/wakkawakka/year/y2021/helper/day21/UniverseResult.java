package online.wakkawakka.year.y2021.helper.day21;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.math.BigInteger;

@Getter
@AllArgsConstructor
public class UniverseResult implements Serializable {
    private String id;
    private final BigInteger p1Wins;
    private final BigInteger p2Wins;
}
