package online.wakkawakka.year.y2021.helper.day09;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class FloorPoint {
    private int depth;
    private boolean inBasin;
    private int row;
    private int column;

    public FloorPoint(int row, int column, int depth) {
        this.row = row;
        this.column = column;
        this.depth = depth;
        this.inBasin = false;
    }

    public FloorPoint(int depth) {
        this.depth = depth;
        this.inBasin = false;
    }
}
