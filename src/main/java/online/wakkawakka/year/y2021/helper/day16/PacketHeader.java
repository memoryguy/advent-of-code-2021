package online.wakkawakka.year.y2021.helper.day16;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Getter
@Setter
public class PacketHeader {
    private static final Logger logger = LoggerFactory.getLogger(PacketHeader.class);

    private int version;
    private int typeId;

    public PacketHeader(String inputBits) {
        this.version = Integer.parseInt(inputBits.substring(0, 3), 2);
        this.typeId = Integer.parseInt(inputBits.substring(3, 6), 2);

        logger.debug("Packet version: {}", this.version);
        logger.debug("Packet type: {}", this.typeId);
    }
}
