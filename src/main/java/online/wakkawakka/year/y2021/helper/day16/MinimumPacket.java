package online.wakkawakka.year.y2021.helper.day16;

public class MinimumPacket extends OperatorPacket {
    public MinimumPacket(PacketHeader header, String inputBits) {
        super(header, inputBits);
    }

    @Override
    public long getValue() {
        return subPackets.stream().mapToLong(Packet::getValue).min().orElse(0);
    }
}
