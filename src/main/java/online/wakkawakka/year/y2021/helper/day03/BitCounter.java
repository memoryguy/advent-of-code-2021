package online.wakkawakka.year.y2021.helper.day03;

public class BitCounter {
    private int zeroCount;
    private int oneCount;

    public BitCounter() {
        this.zeroCount = 0;
        this.oneCount = 0;
    }

    public void input(char bit) throws IllegalArgumentException {
        if (bit == '0') {
            zeroCount++;
        } else if (bit == '1') {
            oneCount++;
        } else {
            throw new IllegalArgumentException("Unknown bit: " + bit);
        }
    }

    public char getMostCommon() {
        return zeroCount > oneCount ? '0' : '1';
    }

    public char getLeastCommon() {
        return getMostCommon() == '0' ? '1' : '0';
    }

    public void reset() {
        this.zeroCount = 0;
        this.oneCount = 0;
    }
}
