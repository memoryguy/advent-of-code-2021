package online.wakkawakka.year.y2021.helper.day16;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LiteralIntegerPacket extends Packet {
    private static final Logger logger = LoggerFactory.getLogger(LiteralIntegerPacket.class);

    private String valueBits = "";
    private long value;

    public LiteralIntegerPacket(PacketHeader header, String inputBits) {
        super(header, inputBits);

        boolean continueReading = true;

        while (continueReading) {
            if (inputBits.charAt(bitsConsumed++) == '0') {
                continueReading = false;
            }

            valueBits += inputBits.substring(bitsConsumed, Math.min(bitsConsumed+4, inputBits.length()));

            bitsConsumed += 4;
        }
    }

    public long getValue() {
        try {
            logger.debug("getting value of: {}", this.valueBits);
            logger.debug("# bits: {}", this.valueBits.length());
            return Long.parseLong(this.valueBits, 2);
        } catch (NumberFormatException e) {
            logger.error("Unable to parse number", e);
            return -1;
        }
    }
}
