package online.wakkawakka.year.y2021.helper.day04;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BingoCard {
    private static final Logger logger = LoggerFactory.getLogger(BingoCard.class);

    private List<List<CardPosition>> board = new ArrayList<>();

    public BingoCard(List<String> lines) throws NumberFormatException {
        // Bingo Cards should be 5x5. Scream and yell if we didn't get the
        // right number of input
        if (lines.size() != 5) {
            throw new IllegalArgumentException("Incorrect board size: " + lines.size());
        }

        // each line of the card is a whitespace-separated list of numbers.
        // Split on whitespace, convert to a CardPosition, and put in a list.
        // The list is added as the row of data
        lines.forEach(line ->
            board.add(Arrays.stream(line.split("\\s+"))
                    .map(CardPosition::new)
                    .collect(Collectors.toList()))
        );
    }

    public void markNumber(int value) {
        // given a number, find if it exists on the card and mark it selected,
        // if present. If the number is not present, do nothing
        board.forEach(r -> r.stream().filter(c -> c.getValue() == value).findFirst().ifPresent(e -> e.setSelected(true)));
    }

    public boolean isWinner() {
        int[] selectCount = new int[board.get(0).size()];

        // check for complete row
        if (board.stream().anyMatch(r -> r.stream().allMatch(CardPosition::isSelected))) {
            return true;
        }

        // check for complete column: count the number of selected numbers in
        // each column. If any are 5 (or, all numbers in the column are
        // selected), then we have a winner
        board.forEach(r -> {
            for (int c = 0; c < r.size(); c++) {
                if (r.get(c).isSelected()) {
                    selectCount[c]++;
                }
            }
        });

        for (int count : selectCount) {
            if (count == board.size()) {
                return true;
            }
        }

        return false;
    }

    public int sumAllUnmarked() {
        // go through each row and get the sum of the unmarked/unselected
        // numbers. Then sum all those sums to get a total of all the unmarked
        // numbers on the card
        return board.stream()
                .map(r -> r.stream()
                        .filter(c -> !c.isSelected())
                        .map(CardPosition::getValue)
                        .reduce(0, Integer::sum))
                .reduce(0, Integer::sum);
    }

    @Override
    public String toString() {
        return board.stream()
                .map(row -> row.stream()
                        .map(CardPosition::toString)
                        .collect(Collectors.joining(" ")))
                .collect(Collectors.joining("\n"));
    }
}
