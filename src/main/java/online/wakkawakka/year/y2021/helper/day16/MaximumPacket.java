package online.wakkawakka.year.y2021.helper.day16;

public class MaximumPacket extends OperatorPacket {
    public MaximumPacket(PacketHeader header, String inputBits) {
        super(header, inputBits);
    }

    @Override
    public long getValue() {
        return subPackets.stream().mapToLong(Packet::getValue).max().orElse(0);
    }
}
