package online.wakkawakka.year.y2021.helper.day21;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class Wins {
    private BigInteger player1Wins;
    private BigInteger player2Wins;

    public Wins add(Wins other) {
        return new Wins(this.player1Wins.add(other.player1Wins), this.player2Wins.add(other.player2Wins));
    }

    public Wins multiply(BigInteger probability) {
        return new Wins(this.player1Wins.multiply(probability), this.player2Wins.multiply(probability));
    }
}
