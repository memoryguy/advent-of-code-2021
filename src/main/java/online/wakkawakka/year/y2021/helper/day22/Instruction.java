package online.wakkawakka.year.y2021.helper.day22;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@Setter
@ToString
public class Instruction {
    private static final Logger logger = LoggerFactory.getLogger(Instruction.class);
    private static final Pattern inputPattern = Pattern.compile("^(?<state>.+?) x=(?<xlow>[\\d-]+)\\.\\.(?<xhigh>[\\d-]+),y=(?<ylow>[\\d-]+)\\.\\.(?<yhigh>[\\d-]+),z=(?<zlow>[\\d-]+)\\.\\.(?<zhigh>[\\d-]+)$");
    
    private boolean on;
    private int xLow;
    private int xHigh;
    private int yLow;
    private int yHigh;
    private int zLow;
    private int zHigh;
    
    public Instruction(String input) throws NumberFormatException {
        Matcher matcher = inputPattern.matcher(input);
        
        if (matcher.find()) {
            this.on = matcher.group("state").equals("on");
            this.xLow = Integer.parseInt(matcher.group("xlow"));
            this.xHigh = Integer.parseInt(matcher.group("xhigh"));
            this.yLow = Integer.parseInt(matcher.group("ylow"));
            this.yHigh = Integer.parseInt(matcher.group("yhigh"));
            this.zLow = Integer.parseInt(matcher.group("zlow"));
            this.zHigh = Integer.parseInt(matcher.group("zhigh"));
        }
    }

    public boolean inRangeA() {
        return (this.xLow >= -50 && this.xHigh <= 50 && this.yLow >= -50 && this.yHigh <= 50 && this.zLow >= -50 && this.zHigh <= 50);
    }
}
