package online.wakkawakka.year;

import lombok.Getter;
import online.wakkawakka.model.ExpectedResult;
import online.wakkawakka.model.ExpectedResults;
import online.wakkawakka.util.Utility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class DayBase {
    public static final long NO_SOLUTION = -999999999999L;

    private static final Logger logger = LoggerFactory.getLogger(DayBase.class);

    protected int year;
    protected int day;
    protected String part;
    protected boolean sampleData;
    protected String variant;
    protected List<String> inputLines;

    private ExpectedResults expectedResults;

    protected DayBase(Integer year, Integer day, String part, Boolean sampleData, String variant) throws IOException, NumberFormatException {
        this.year = year;
        this.day = day;
        this.part = part;
        this.sampleData = sampleData;
        this.inputLines = new ArrayList<>();
        this.variant = variant;

        this.expectedResults = Utility.getExpectedResults(year);

        initializeMembers();

        loadInputData();
    }

    public final Optional<Long> getExpectedResult() {
        Optional<ExpectedResult> result;

        if (this.sampleData) {
            result = this.expectedResults.getSample().stream()
                    .filter(er -> er.getDay() == this.day && er.getPart().equals(this.part) && (er.getVariant() == null || er.getVariant().equals(this.variant)))
                    .findFirst();
        } else {
            result = this.expectedResults.getTest().stream()
                    .filter(er -> er.getDay() == this.day && er.getPart().equals(this.part) && (er.getVariant() == null || er.getVariant().equals(this.variant)))
                    .findFirst();
        }

        return result.map(ExpectedResult::getExpected);
    }

    protected void initializeMembers() {

    }

    private void loadInputData() throws IOException, NumberFormatException {
        try (BufferedReader reader = Utility.getInputFileBuffer(year, day, "", sampleData, variant)) {
            String inputLine;

            while ((inputLine = reader.readLine()) != null) {
                processInputLine(inputLine.trim());
            }
        }
    }

    protected void processInputLine(String inputLine) throws NumberFormatException {
        this.inputLines.add(inputLine);
    }

    public final long runPart(String part) {
        List<String> description = Utility.getProblemDescription(year, day, part);

        logger.info("Begin executing challenge: Day {} Part {} {}", day, part, sampleData ? "Using sample data" : "");

        if (!description.isEmpty()) {
            logger.info("PROBLEM DESCRIPTION:");
            description.forEach(logger::info);
        }

        if (part.equalsIgnoreCase("a")) {
            return partA();
        } else {
            return partB();
        }
    }

    public long partA() {
        return NO_SOLUTION;
    }

    public long partB() {
        return NO_SOLUTION;
    }
}
