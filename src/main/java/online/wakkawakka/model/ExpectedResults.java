package online.wakkawakka.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ExpectedResults {
    List<ExpectedResult> sample;
    List<ExpectedResult> test;
}
