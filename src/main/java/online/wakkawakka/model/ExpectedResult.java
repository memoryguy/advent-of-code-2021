package online.wakkawakka.model;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpectedResult {
    private int day;
    private String part;
    @JsonSetter(nulls= Nulls.AS_EMPTY)
    private String variant;
    private long expected;
}
