# Advent of Code 2021 Solutions

## Introduction
These are my solutions to the the 2021 Advent of Code. The framework allows any day's challenge to be executed by
specifying the day number and part (A/B). An optional boolean parameter allows specifying whether to run with sample
data.

