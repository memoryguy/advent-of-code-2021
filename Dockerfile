FROM amazoncorretto:18-alpine3.15-jdk as build

RUN apk add maven

WORKDIR /app
COPY pom.xml /app/pom.xml

RUN mvn -B -e -C -T 1C org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline

COPY src/ /app/src/

RUN mvn package

FROM amazoncorretto:18-alpine3.15 as final

ENTRYPOINT [ "java", "-jar", "/app.jar" ]

COPY --from=build /app/target/aoc2021-1.0.jar /app.jar
