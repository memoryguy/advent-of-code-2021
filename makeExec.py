#!/usr/bin/python3

# Construct IntelliJ Run Configurations for each of the AoC challenges. Run the script, copy the XML blocks into the .idea/workspace.xml file

for day in range(1, 25):
    for part in ('a', 'b'):
        for data in ('--sampleData', ''):
            if data == '--sampleData':
                dataSource = 'sample'
            else:
                dataSource = 'test'

            print(f"""    <configuration name="AdventOfCode Day {day:02d}{part} {dataSource}" type="Application" factoryName="Application" folderName="Day {day:02d}">
              <option name="MAIN_CLASS_NAME" value="online.wakkawakka.AdventOfCode2021" />
              <module name="aoc2021" />
              <option name="PROGRAM_PARAMETERS" value="--day={day} --part={part} {data}" />
              <method v="2">
                <option name="Make" enabled="true" />
              </method>
            </configuration>
        """)

print("")


for day in range(1, 25):
    for part in ('a', 'b'):
        for data in ('--sampleData', ''):
            if data == '--sampleData':
                dataSource = 'sample'
            else:
                dataSource = 'test'

            print(f"      <item itemvalue=\"Application.AdventOfCode Day {day:02d}{part} {dataSource}\" />")